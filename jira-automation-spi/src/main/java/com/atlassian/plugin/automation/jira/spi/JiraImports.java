package com.atlassian.plugin.automation.jira.spi;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Transitive imports
 */
@Named
@SuppressWarnings("unused")
public class JiraImports
{
    private final PermissionManager permissionManager;
    private final EventPublisher eventPublisher;
    private final GlobalPermissionManager globalPermissionManager;

    @Inject
    public JiraImports(
            @ComponentImport PermissionManager permissionManager,
            @ComponentImport EventPublisher eventPublisher,
            @ComponentImport GlobalPermissionManager globalPermissionManager
            )
    {
        this.permissionManager = permissionManager;
        this.eventPublisher = eventPublisher;
        this.globalPermissionManager = globalPermissionManager;
    }
}
