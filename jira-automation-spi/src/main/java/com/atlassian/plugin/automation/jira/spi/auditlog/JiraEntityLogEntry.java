package com.atlassian.plugin.automation.jira.spi.auditlog;

import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Locale;

public class JiraEntityLogEntry
{
    private static final String ALL_KEY = "all";
    private static final String LAST_KEY = "last";

    // this is here because the standard ISODateFormatter from joda.time didn't seem to work with lucene
    private static DateTimeFormatter ISO_DATE_FORMAT = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZ").withLocale(Locale.US);

    private final JSONObject last;
    private final JSONArray all;
    private DateTime timestamp;
    private String entry;

    public JiraEntityLogEntry(DateTime timestamp, String entry, String existingEntry)
    {
        this.timestamp = timestamp;
        this.entry = entry;

        this.last = createJSONEntry(timestamp, entry);
        final JSONArray jsonArray = new JSONArray();
        jsonArray.put(this.last);
        this.all = StringUtils.isBlank(existingEntry) ? jsonArray : parseExistingArray(existingEntry);
    }

    public JiraEntityLogEntry(DateTime timestamp, String logEntry)
    {
        this(timestamp, logEntry, null);
    }

    public String getJSONString()
    {
        try
        {
            return new JSONObject().put(LAST_KEY, last).put(ALL_KEY, all).toString();
        }
        catch (JSONException e)
        {
            return "{}";
        }
    }

    private JSONArray parseExistingArray(final String existingJson)
    {
        JSONArray jsonArray = new JSONArray();
        try
        {
            jsonArray = new JSONObject(existingJson).optJSONArray(ALL_KEY);
        }
        catch (JSONException e)
        {
            // swallow the exception
        }
        jsonArray.put(createJSONEntry(timestamp, entry));
        return jsonArray;
    }

    private static JSONObject createJSONEntry(DateTime when, String entry)
    {
        try
        {
            return new JSONObject().put("date", when.toString(ISO_DATE_FORMAT)).put("entry", entry);
        }
        catch (JSONException e)
        {
            return new JSONObject();
        }
    }
}
