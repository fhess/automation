package com.atlassian.plugin.automation.jira.trigger;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.plugin.automation.config.DefaultAutomationConfiguration;
import com.atlassian.plugin.automation.core.TriggerContext;
import com.atlassian.plugin.automation.jira.spi.scheduler.JiraCronExpressionValidator;
import com.atlassian.plugin.automation.jira.util.JqlMatcherService;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.query.Query;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.internal.verification.Times;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Map;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestJqlFilterTrigger
{
    @Mock
    private SearchService searchService;
    @Mock
    private UserManager userManager;
    @Mock
    private IssueService issueService;
    @Mock
    private SoyTemplateRenderer soyTemplateRenderer;
    @Mock
    private I18nResolver i18nResolver;
    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private DateTimeFormatter dateTimeFormatter;
    @Mock
    private JqlMatcherService jqlMatcherService;
    @Mock
    private JiraCronExpressionValidator cronExpressionValidator;

    @Before
    public void setUp() throws Exception
    {
        when(i18nResolver.getText(anyString())).thenReturn("translated text");
        ApplicationUser user = mock(ApplicationUser.class);
        when(userManager.getUserByName(anyString())).thenReturn(user);

        final Query query = mock(Query.class);
        final SearchService.ParseResult parseResult = new SearchService.ParseResult(query, new MessageSetImpl());
        when(searchService.parseQuery(any(ApplicationUser.class), anyString())).thenReturn(parseResult);
        when(searchService.validateQuery((ApplicationUser) Matchers.any(), (Query) any())).thenReturn(new MessageSetImpl());
        when(jqlMatcherService.validateJql(anyString(), anyString(), anyString())).thenReturn(mock(ErrorCollection.class));

        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_SEARCH_VIEWS_MAX_LIMIT)).thenReturn("10");
        when(cronExpressionValidator.isValidExpression(anyString())).thenReturn(true);
    }

    @Test
    public void testGetItems() throws Exception
    {
        final JqlFilterTrigger jqlFilterTrigger = new JqlFilterTrigger(searchService, userManager, applicationProperties,
                soyTemplateRenderer, dateTimeFormatter, cronExpressionValidator, jqlMatcherService);
        final Map<String, List<String>> params = Maps.newHashMap();
        final String jql = "project=TEST";
        params.put(JqlFilterTrigger.JQL_KEY, Lists.newArrayList(jql));

        List<Issue> issues = Lists.newArrayList(mock(Issue.class));
        SearchResults searchResults = new SearchResults(issues, PagerFilter.getUnlimitedFilter());
        when(searchService.search(any(ApplicationUser.class), any(Query.class), any(PagerFilter.class))).thenReturn(searchResults);

        jqlFilterTrigger.init(new DefaultAutomationConfiguration(-1, "", params));
        ErrorCollection errorCollection = new ErrorCollection();
        final Iterable<Issue> result = jqlFilterTrigger.getItems(new TriggerContext("actor"), errorCollection);
        assertEquals("invalid number of issues", 1, Iterables.size(result));

        assertFalse("errors present", errorCollection.hasAnyErrors());
        verify(applicationProperties, new Times(1)).getDefaultBackedString(anyString());
    }

    @Test
    public void testSearchException() throws Exception
    {
        final JqlFilterTrigger jqlFilterTrigger = new JqlFilterTrigger(searchService, userManager, applicationProperties,
                soyTemplateRenderer, dateTimeFormatter, cronExpressionValidator, jqlMatcherService);
        final Map<String, List<String>> params = Maps.newHashMap();
        final String jql = "project=TEST";
        params.put(JqlFilterTrigger.JQL_KEY, Lists.newArrayList(jql));

        when(searchService.search(any(ApplicationUser.class), any(Query.class), any(PagerFilter.class))).thenThrow(SearchException.class);

        jqlFilterTrigger.init(new DefaultAutomationConfiguration(-1, "", params));
        ErrorCollection errorCollection = new ErrorCollection();
        final Iterable<Issue> result = jqlFilterTrigger.getItems(new TriggerContext("actor"), errorCollection);
        assertEquals("invalid number of issues", 0, Iterables.size(result));

        assertTrue("no errors present", errorCollection.hasAnyErrors());
    }

    @Test
    public void testValidateAddConfiguration() throws Exception
    {
        final JqlFilterTrigger jqlFilterTrigger = new JqlFilterTrigger(searchService, userManager, applicationProperties,
                soyTemplateRenderer, dateTimeFormatter, cronExpressionValidator, jqlMatcherService);
        final Map<String, List<String>> params = Maps.newHashMap();
        params.put("cronExpression", Lists.newArrayList("*/5 * * * * ?"));

        assertTrue("no errors present", jqlFilterTrigger.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        params.put(JqlFilterTrigger.JQL_KEY, Lists.newArrayList("project=TEST"));
        assertFalse("errors present", jqlFilterTrigger.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        params.put(JqlFilterTrigger.MAX_RESULTS_KEY, Lists.newArrayList("a"));
        assertTrue("no errors present", jqlFilterTrigger.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        params.put(JqlFilterTrigger.MAX_RESULTS_KEY, Lists.newArrayList("-10"));
        assertTrue("no errors present", jqlFilterTrigger.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        params.put(JqlFilterTrigger.MAX_RESULTS_KEY, Lists.newArrayList("10"));
        assertFalse("errors present", jqlFilterTrigger.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());

        when(cronExpressionValidator.isValidExpression(anyString())).thenReturn(false);
        assertTrue("errors present", jqlFilterTrigger.validateAddConfiguration(i18nResolver, params, "").hasAnyErrors());
    }
}
