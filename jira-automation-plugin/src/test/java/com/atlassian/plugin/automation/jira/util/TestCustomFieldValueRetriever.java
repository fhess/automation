package com.atlassian.plugin.automation.jira.util;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TestCustomFieldValueRetriever
{

    @Mock
    private CustomFieldManager customFieldManager;

    @Mock
    private Issue issue;


    @Before
    public void setUp() throws Exception
    {

        Mockito.when(customFieldManager.getCustomFieldObject(10000L)).thenReturn(mock(CustomField.class));
        Mockito.when(customFieldManager.getCustomFieldObject(20000L)).thenReturn(null);
        Mockito.when(issue.getCustomFieldValue(Matchers.any(CustomField.class))).thenReturn("Some value");

    }

    @Test
    public void testCustomFieldFound()
    {
        CustomFieldValueRetriever valueRetriever = new CustomFieldValueRetriever(customFieldManager, issue);
        String value = (String) valueRetriever.get(10000);

        assertEquals("Values don't match", "Some value", value);
    }

    @Test
    public void testCustomFieldNotFound()
    {

        CustomFieldValueRetriever valueRetriever = new CustomFieldValueRetriever(customFieldManager, issue);
        String value = (String) valueRetriever.get(20000);

        assertNull("Value is not null", value);
    }

}
