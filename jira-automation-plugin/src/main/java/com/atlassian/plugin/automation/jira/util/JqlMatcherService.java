package com.atlassian.plugin.automation.jira.util;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.jql.parser.JqlParseException;
import com.atlassian.jira.jql.parser.JqlQueryParser;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.plugin.automation.core.TriggerContext;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.query.Query;
import com.atlassian.sal.api.message.I18nResolver;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class JqlMatcherService
{
    private static final Logger log = LoggerFactory.getLogger(JqlMatcherService.class);
    
    private final JqlQueryParser jqlQueryParser;
    private final IssueManager issueManager;
    private final SearchService searchService;
    private final UserManager userManager;
    private final I18nResolver i18nResolver;

    @Inject
    public JqlMatcherService(
            @ComponentImport final JqlQueryParser jqlQueryParser,
            @ComponentImport final SearchService searchService,
            @ComponentImport final UserManager userManager,
            @ComponentImport final IssueManager issueManager,
            @ComponentImport final I18nResolver i18nResolver)
    {
        this.jqlQueryParser = jqlQueryParser;
        this.searchService = searchService;
        this.userManager = userManager;
        this.issueManager = issueManager;
        this.i18nResolver = i18nResolver;
    }

    /**
     * @return Issue with given id if it matches the JQL constraint, or null if no issue is matched
     */
    public Issue getMatchedIssue(final TriggerContext context, final ErrorCollection errorCollection, final String jql, final Long issueId)
    {
        // get the latest version of the issue in case anything has changed since the event was fired
        final Issue issue = issueManager.getIssueObject(issueId);

        // safety check - if jql is empty or no issue with this id exists, return
        if (StringUtils.isBlank(jql) || issue == null)
        {
            return issue;
        }

        try
        {
            final ApplicationUser user = userManager.getUserByName(context.getActor());
            final Query query = jqlQueryParser.parseQuery(jql);
            final Query issueQuery = JqlQueryBuilder.newBuilder().where().issue().eq(issueId).and().addClause(query.getWhereClause()).buildQuery();
            if (log.isDebugEnabled())
            {
                log.debug("Trying to match issue '" + issue.getKey() + "' using query '" + issueQuery + "'");
            }

            final long totalHits = searchService.searchCount(user, issueQuery);

            if (log.isDebugEnabled())
            {
                log.debug("Matched " + totalHits + " issues using query '" + issueQuery + "'");
            }
            if (totalHits > 0)
            {
                return issue;
            }
        }
        catch (JqlParseException e)
        {
            log.error("Error parsing query '" + jql + "'. No issue will be processed.", e);
            errorCollection.addErrorMessage("Error parsing query '" + jql + "'. No issue will be processed.");
        }
        catch (SearchException e)
        {
            log.error("Error parsing query '" + jql + "'. No issue will be processed.", e);
            errorCollection.addErrorMessage("Error parsing query '" + jql + "'. No issue will be processed.");
        }
        return null;
    }
    
    public ErrorCollection validateJql(final String actor, final String jqlKey, final String jqlString)
    {
        final ErrorCollection errors = new ErrorCollection();
        final ApplicationUser user = userManager.getUserByName(actor);
        final SearchService.ParseResult parseResult = searchService.parseQuery(user, jqlString);

        if (!parseResult.isValid())
        {
            errors.addError(jqlKey, i18nResolver.getText("automation.jira.jql.invalid"));
        }
        else
        {
            // Validation is now here, because here we still have HTTP session to validate the JQL (see JRA-33499)
            final MessageSet messageSet = searchService.validateQuery(user, parseResult.getQuery());
            if (messageSet.hasAnyErrors())
            {
                errors.addError(jqlKey, messageSet.getErrorMessages().toString());
            }
            else if (messageSet.hasAnyWarnings())
            {
                errors.addError(jqlKey, messageSet.getWarningMessages().toString());
            }
        }
        return errors;
    }
    
}
