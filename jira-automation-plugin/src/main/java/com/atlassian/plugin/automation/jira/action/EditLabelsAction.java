package com.atlassian.plugin.automation.jira.action;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.label.Label;
import com.atlassian.jira.issue.label.LabelManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugin.automation.core.Action;
import com.atlassian.plugin.automation.core.action.ActionConfiguration;
import com.atlassian.plugin.automation.core.auditlog.AuditString;
import com.atlassian.plugin.automation.core.auditlog.DefaultAuditString;
import com.atlassian.plugin.automation.jira.util.Constants;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.plugin.automation.util.ParameterUtil;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.plugin.automation.util.ParameterUtil.singleValue;

@SuppressWarnings("Duplicates")
@Scanned
public class EditLabelsAction implements Action<Issue>
{
    public static final String EDIT_LABELS_ADD_KEY = "jiraEditLabelsAddField";
    public static final String EDIT_LABELS_REMOVE_KEY = "jiraEditLabelsRemoveField";
    public static final String EDIT_LABLES_NOTIFICATION_KEY = "jiraEditLabelsNotification";
    public static final String EDIT_LABLES_CASEINSENSITIVE_KEY = "jiraHandleLabelsCaseInsensitive";

    private static final Logger log = LoggerFactory.getLogger(EditLabelsAction.class);
    public static final String ADDED_LABELS = "addedLabels";
    public static final String REMOVED_LABELS = "removedLabels";

    private final UserManager userManager;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final LabelManager labelManager;
    private boolean sendNotification;
    private boolean handleLabelsCaseInsensitive;
    private Set<String> labelsToAdd;
    private Set<String> labelsToRemove;

    private static final Function<String, String> TO_UPPER = new Function<String, String>()
    {
        public String apply(String input)
        {
            return input.toUpperCase();
        }
    };

    @Inject
    public EditLabelsAction(
            @ComponentImport final UserManager userManager,
            @ComponentImport final SoyTemplateRenderer soyTemplateRenderer,
            @ComponentImport final LabelManager labelManager)
    {
        this.userManager = userManager;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.labelManager = labelManager;
    }

    @Override
    public void init(ActionConfiguration config)
    {
        labelsToAdd = getLabels(config, EDIT_LABELS_ADD_KEY);
        labelsToRemove = getLabels(config, EDIT_LABELS_REMOVE_KEY);
        sendNotification = Boolean.parseBoolean(singleValue(config, EDIT_LABLES_NOTIFICATION_KEY));
        handleLabelsCaseInsensitive = Boolean.parseBoolean(singleValue(config, EDIT_LABLES_CASEINSENSITIVE_KEY));
    }

    @Override
    public void execute(String actor, Iterable<Issue> issues, ErrorCollection errorCollection)
    {
        final ApplicationUser actorUser = userManager.getUserByName(actor);
        for (Issue issue : issues)
        {
            final Set<Label> labels = labelManager.getLabels(issue.getId());
            final Set<String> originalLabels = Sets.newHashSet(
                    Iterables.transform(labels, new Function<Label, String>()
                    {
                        @Override
                        public String apply(@Nullable Label label)
                        {
                            return (label != null) ? label.getLabel() : "";
                        }
                    }));
            Set<String> modifiedLabels = Sets.newHashSet(originalLabels);
            if (handleLabelsCaseInsensitive && !labelsToRemove.isEmpty())
            {
                final Set<String> upperCaseRemoveLabels = Sets
                        .newHashSet(Collections2.transform(labelsToRemove, TO_UPPER));

                modifiedLabels = Sets.filter(modifiedLabels, new Predicate<String>()
                {
                    public boolean apply(String input)
                    {
                        return !upperCaseRemoveLabels.contains(input.toUpperCase());
                    }
                });
            }
            else
            {
                modifiedLabels.removeAll(labelsToRemove);
            }
            log.debug("originalLabels: " + ReflectionToStringBuilder.toString(originalLabels.toArray()));
            log.debug("modifiedLabels: " + ReflectionToStringBuilder.toString(modifiedLabels.toArray()));
            modifiedLabels.addAll(labelsToAdd);
            // If there is a difference we run the change
            if (!Sets.symmetricDifference(originalLabels, modifiedLabels).isEmpty())
            {
                if (log.isDebugEnabled())
                {
                    log.debug("Found difference setting labels {} issue {}",
                            ReflectionToStringBuilder.toString(modifiedLabels.toArray()), issue.getKey());
                }
                labelManager.setLabels(actorUser, issue.getId(), modifiedLabels, sendNotification, true);
            }
        }
    }

    @Override
    public AuditString getAuditLog()
    {
        return new DefaultAuditString(String.format("Edited labels on issue. Added: '%s' Removed: '%s'",
                StringUtils.join(labelsToAdd, ","), StringUtils.join(labelsToRemove, ",")));
    }

    @Override
    public String getConfigurationTemplate(ActionConfiguration actionConfiguration, String actor)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, actionConfiguration);
            context.put(ADDED_LABELS, Lists.newArrayList(getLabels(actionConfiguration, EDIT_LABELS_ADD_KEY)));
            context.put(REMOVED_LABELS, Lists.newArrayList(getLabels(actionConfiguration, EDIT_LABELS_REMOVE_KEY)));

            return soyTemplateRenderer.render(Constants.CONFIG_COMPLETE_KEY, "Atlassian.Templates.Automation.JIRA.editLabelAction", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public String getViewTemplate(final ActionConfiguration actionConfiguration, final String actor)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, actionConfiguration);
            context.put(ADDED_LABELS, Lists.newArrayList(getLabels(actionConfiguration, EDIT_LABELS_ADD_KEY)));
            context.put(REMOVED_LABELS, Lists.newArrayList(getLabels(actionConfiguration, EDIT_LABELS_REMOVE_KEY)));

            return soyTemplateRenderer.render(Constants.CONFIG_COMPLETE_KEY, "Atlassian.Templates.Automation.JIRA.editLabelActionView", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public ErrorCollection validateAddConfiguration(I18nResolver i18n, Map<String, List<String>> params, String actor)
    {
        final ErrorCollection errors = new ErrorCollection();
        if ((!params.containsKey(EDIT_LABELS_ADD_KEY) || StringUtils.isBlank(singleValue(params, EDIT_LABELS_ADD_KEY))) &&
                (!params.containsKey(EDIT_LABELS_REMOVE_KEY) || StringUtils.isBlank(singleValue(params, EDIT_LABELS_REMOVE_KEY))))
        {
            errors.addError(EDIT_LABELS_ADD_KEY, i18n.getText("automation.jira.editLabels.empty"));
            errors.addError(EDIT_LABELS_REMOVE_KEY, i18n.getText("automation.jira.editLabels.empty"));

            return errors;
        }

        if (params.containsKey(EDIT_LABELS_ADD_KEY))
        {
            for (String label : params.get(EDIT_LABELS_ADD_KEY))
            {
                if (StringUtils.isBlank(label))
                {
                    errors.addError(EDIT_LABELS_ADD_KEY, i18n.getText("automation.jira.editLabels.invalid"));
                    break;
                }
            }
        }

        if (params.containsKey(EDIT_LABELS_REMOVE_KEY))
        {
            for (String label : params.get(EDIT_LABELS_REMOVE_KEY))
            {
                if (StringUtils.isBlank(label))
                {
                    errors.addError(EDIT_LABELS_REMOVE_KEY, i18n.getText("automation.jira.editLabels.invalid"));
                    break;
                }
            }
        }
        return errors;
    }

    private Set<String> getLabels(final ActionConfiguration config, final String configKey)
    {
        final Set<String> ret = Sets.newHashSet();
        if (config != null && config.getParameters().containsKey(configKey))
        {
            for (String label : config.getParameters().get(configKey))
            {
                if (StringUtils.isNotBlank(label))
                {
                    ret.add(label);
                }
            }
        }
        return ret;
    }
}
