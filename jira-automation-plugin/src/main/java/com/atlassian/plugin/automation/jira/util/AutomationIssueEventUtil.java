package com.atlassian.plugin.automation.jira.util;

import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.automation.core.AutomationConfiguration;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @author: mkonecny
 */
public class AutomationIssueEventUtil
{
    public static Set<Long> transformEventIds(final List<String> eventIdsList)
    {
        final Set<Long> ret = Sets.newHashSet();
        if (eventIdsList != null)
        {
            for (String eventIdString : eventIdsList)
            {
                if (StringUtils.isNotBlank(eventIdString) && StringUtils.isNumeric(eventIdString))
                {
                    ret.add(Long.parseLong(eventIdString));
                }
            }
        }
        return ret;
    }

    public static Iterable getRenderableEvents(Collection<EventType> eventTypes, AutomationConfiguration config, String configKey, final ApplicationUser user)
    {
        Set<Long> eventIds = Sets.newHashSet();
        if (config != null && config.getParameters().containsKey(configKey))
        {
            eventIds = AutomationIssueEventUtil.transformEventIds(config.getParameters().get(configKey));
        }

        final Set<Long> finalEventIds = eventIds;
        return Iterables.transform(eventTypes, new Function<EventType, RenderableEvent>()
        {
            @Override
            public RenderableEvent apply(@Nullable final EventType input)
            {
                if (input != null)
                {
                    return new RenderableEvent(input.getId(), input.getTranslatedName(user), finalEventIds.contains(input.getId()));
                }
                return null;
            }
        });
    }
}
