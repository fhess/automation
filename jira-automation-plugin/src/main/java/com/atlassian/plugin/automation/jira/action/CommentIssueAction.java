package com.atlassian.plugin.automation.jira.action;

import com.atlassian.fugue.Either;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.issue.comment.CommentService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.plugin.automation.core.Action;
import com.atlassian.plugin.automation.core.action.ActionConfiguration;
import com.atlassian.plugin.automation.core.auditlog.AuditString;
import com.atlassian.plugin.automation.core.auditlog.DefaultAuditString;
import com.atlassian.plugin.automation.jira.util.ErrorCollectionUtil;
import com.atlassian.plugin.automation.jira.util.SecurityLevelContextProvider;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.plugin.automation.util.ParameterUtil;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.templaterenderer.RenderingException;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

import static com.atlassian.plugin.automation.jira.util.Constants.CONFIG_COMPLETE_KEY;
import static com.atlassian.plugin.automation.util.ParameterUtil.singleValue;

/**
 * Implements comment on issue
 */
@Scanned
public class CommentIssueAction implements Action<Issue>
{
    public static final String COMMENT_KEY = "jiraComment";
    public static final String COMMENT_NOTIFICATION_KEY = "jiraCommentNotification";
    public static final String COMMENT_SECURITY_KEY = "jiraCommentSecurityLevel";

    private static final Logger log = Logger.getLogger(CommentIssueAction.class);
    private static final int MAX_COMMENT_LENGTH = 60;

    private final CommentService commentService;
    private final UserManager userManager;
    private final IssueService issueService;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final TemplateRenderer templateRenderer;
    private final SecurityLevelContextProvider securityLevelContextProvider;
    private String comment;
    private Either<String, Long> securityLevel;
    private boolean sendNotification;

    @Inject
    public CommentIssueAction(
            @ComponentImport final CommentService commentService,
            @ComponentImport final UserManager userManager,
            @ComponentImport final IssueService issueService,
            @ComponentImport final SoyTemplateRenderer soyTemplateRenderer,
            @ComponentImport final TemplateRenderer templateRenderer,
            final SecurityLevelContextProvider securityLevelContextProvider)
    {
        this.commentService = commentService;
        this.userManager = userManager;
        this.issueService = issueService;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.templateRenderer = templateRenderer;
        this.securityLevelContextProvider = securityLevelContextProvider;
    }

    @Override
    public void init(ActionConfiguration config)
    {
        comment = singleValue(config, COMMENT_KEY);
        securityLevel = securityLevelContextProvider.extractValue(singleValue(config, COMMENT_SECURITY_KEY));
        sendNotification = Boolean.parseBoolean(singleValue(config, (COMMENT_NOTIFICATION_KEY)));
    }

    @Override
    public void execute(String actor, Iterable<Issue> items, ErrorCollection errorCollection)
    {
        log.debug("Processing issues");
        final ApplicationUser user = userManager.getUserByName(actor);
        for (Issue issue : items)
        {
            log.debug("Processing the issue: " + issue.getKey());
            final Issue mutableIssue = issueService.getIssue(user, issue.getId()).getIssue();
            com.atlassian.jira.util.ErrorCollection jiraErrorCollection = new SimpleErrorCollection();

            // Render the fragment
            final Map<String, Object> context = Maps.newHashMap();
            context.put("issue", issue);
            context.put("reporter", issue.getReporter());
            context.put("project", issue.getProjectObject());

            try
            {
                final String renderedComment = templateRenderer.renderFragment(comment, context);
                if (securityLevel != null)
                {
                    if (securityLevel.isLeft())
                    {
                        commentService.create(user, mutableIssue, renderedComment, securityLevel.left().get(), null,
                                sendNotification, jiraErrorCollection);
                    }
                    else if (securityLevel.isRight())
                    {
                        commentService.create(user, mutableIssue, renderedComment, null, securityLevel.right().get(),
                                sendNotification, jiraErrorCollection);
                    }
                }
                else
                {
                    commentService.create(user, mutableIssue, renderedComment, null, null, sendNotification, jiraErrorCollection);
                }
            }
            catch (RenderingException e)
            {
                log.error("Unable to render the comment", e);
                errorCollection.addErrorMessage(String.format("Unable to add comment on issue '%s'", issue.getKey()));
            }

            if (jiraErrorCollection.hasAnyErrors())
            {
                log.error(String.format("Unable to add comment on issue '%s' using actor '%s': %s", issue.getKey(), actor, jiraErrorCollection));
                errorCollection.addErrorCollection(ErrorCollectionUtil.transform(jiraErrorCollection));
            }
        }
    }

    @Override
    public AuditString getAuditLog()
    {
        return new DefaultAuditString(String.format("Comment issue - Adding comment '%s'",
                StringUtils.abbreviateMiddle(comment, "...", MAX_COMMENT_LENGTH)));
    }

    @Override
    public String getConfigurationTemplate(ActionConfiguration config, final String actor)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            String currentSecurityLevel = null;
            ParameterUtil.transformParams(context, config);
            if (config != null)
            {
                currentSecurityLevel = singleValue(config, COMMENT_SECURITY_KEY);
            }
            final Map<String, Object> securityLevelContext = securityLevelContextProvider.getContext(userManager.getUser(actor), null, currentSecurityLevel);
            context.put("securityLevel", securityLevelContext);
            return soyTemplateRenderer.render(CONFIG_COMPLETE_KEY, "Atlassian.Templates.Automation.JIRA.commentAction", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public String getViewTemplate(final ActionConfiguration config, final String actor)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            String currentSecurityLevel = null;
            ParameterUtil.transformParams(context, config);
            if (config != null)
            {
                currentSecurityLevel = singleValue(config, COMMENT_SECURITY_KEY);
            }
            final Map<String, Object> securityLevelContext = securityLevelContextProvider.getContext(userManager.getUserByName(actor), null, currentSecurityLevel);
            context.put("securityLevel", securityLevelContext);
            return soyTemplateRenderer.render(CONFIG_COMPLETE_KEY, "Atlassian.Templates.Automation.JIRA.commentActionView", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public ErrorCollection validateAddConfiguration(I18nResolver i18n, Map<String, List<String>> params, final String actor)
    {
        final ErrorCollection errorCollection = new ErrorCollection();
        if (StringUtils.isBlank(singleValue(params, COMMENT_KEY)))
        {
            errorCollection.addError(COMMENT_KEY, i18n.getText("automation.jira.comment.invalid"));
        }

        final String securityKey = singleValue(params, COMMENT_SECURITY_KEY);
        if (StringUtils.isNotBlank(securityKey))
        {
            errorCollection.addErrorCollection(securityLevelContextProvider.validate(i18n,
                    userManager.getUser(actor), null, securityKey));
        }

        return errorCollection;
    }
}
