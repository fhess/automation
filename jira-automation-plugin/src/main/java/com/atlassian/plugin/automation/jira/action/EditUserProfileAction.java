package com.atlassian.plugin.automation.jira.action;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.UserPropertyManager;
import com.atlassian.plugin.automation.core.Action;
import com.atlassian.plugin.automation.core.action.ActionConfiguration;
import com.atlassian.plugin.automation.core.auditlog.AuditString;
import com.atlassian.plugin.automation.core.auditlog.DefaultAuditString;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.plugin.automation.util.ParameterUtil;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

import static com.atlassian.plugin.automation.jira.util.Constants.CONFIG_COMPLETE_KEY;
import static com.atlassian.plugin.automation.util.ParameterUtil.singleValue;

/**
 * @author: mkonecny
 * Implements editing user profile
 */
@Scanned
public class EditUserProfileAction implements Action<Issue>
{
    public static final String USER_PROPERTY_FIELD_KEY = "editUserProfileKeyField";
    public static final String USER_PROPERTY_VALUE_KEY = "editUserProfileValueField";
    public static final String USER_PROPERTY_SHOULD_OVERWRITE_KEY = "editUserProfileShouldOverwrite";

    private static final Logger log = Logger.getLogger(EditUserProfileAction.class);

    private final UserPropertyManager userPropertyManager;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private String userPropertyKey;
    private String userPropertyValue;
    private boolean shouldOverwriteProperty;
    private int affectedIssues;

    @Inject
    public EditUserProfileAction(@ComponentImport final UserPropertyManager userPropertyManager,
                                 @ComponentImport final SoyTemplateRenderer soyTemplateRenderer)
    {
        this.userPropertyManager = userPropertyManager;
        this.soyTemplateRenderer = soyTemplateRenderer;
    }

    @Override
    public void init(ActionConfiguration config)
    {
        userPropertyKey = singleValue(config, USER_PROPERTY_FIELD_KEY);
        userPropertyValue = singleValue(config, USER_PROPERTY_VALUE_KEY);
        shouldOverwriteProperty = Boolean.parseBoolean(singleValue(config, USER_PROPERTY_SHOULD_OVERWRITE_KEY));
        affectedIssues = 0;
    }

    @Override
    public void execute(String actor, Iterable<Issue> items, ErrorCollection errorCollection)
    {
        for (Issue issue : items)
        {
            if (userPropertyManager.getPropertySet(issue.getReporterUser()).isSettable(userPropertyKey))
            {
                boolean needsUpdate = false;
                final String currentValue = userPropertyManager.getPropertySet(issue.getReporterUser()).getString(userPropertyKey);
                if (currentValue == null || !userPropertyValue.equals(currentValue))
                {
                    // only update if the value is not yet set (current value == null) or if we should overwrite any value
                    needsUpdate = (currentValue == null) || shouldOverwriteProperty;
                }
                if (needsUpdate)
                {
                    userPropertyManager.getPropertySet(issue.getReporterUser()).setString(userPropertyKey, userPropertyValue);
                    affectedIssues++;
                }
            }
            else
            {
                errorCollection.addErrorMessage("Unable to set user property because it is not settable");
            }
        }
    }

    @Override
    public AuditString getAuditLog()
    {
        return new DefaultAuditString(String.format("User property '%s'='%s' updated for %d issues", userPropertyKey, userPropertyValue, affectedIssues));
    }

    @Override
    public String getConfigurationTemplate(ActionConfiguration actionConfiguration, String actor)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, actionConfiguration);

            return soyTemplateRenderer.render(CONFIG_COMPLETE_KEY, "Atlassian.Templates.Automation.JIRA.editUserProfile", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public String getViewTemplate(ActionConfiguration actionConfiguration, String s)
    {
        try
        {
            final Map<String, Object> context = Maps.newHashMap();
            ParameterUtil.transformParams(context, actionConfiguration);

            return soyTemplateRenderer.render(CONFIG_COMPLETE_KEY, "Atlassian.Templates.Automation.JIRA.editUserProfileView", context);
        }
        catch (SoyException e)
        {
            log.error("Error rendering template", e);
            return "Unable to render configuration form. Consult your server logs or administrator.";
        }
    }

    @Override
    public ErrorCollection validateAddConfiguration(I18nResolver i18n, Map<String, List<String>> params, String actor)
    {
        final ErrorCollection errors = new ErrorCollection();
        if (!params.containsKey(USER_PROPERTY_FIELD_KEY) || StringUtils.isBlank(singleValue(params, USER_PROPERTY_FIELD_KEY)))
        {
            errors.addError(USER_PROPERTY_FIELD_KEY, i18n.getText("automation.action.editProfile.field.empty"));
        }

        if (!params.containsKey(USER_PROPERTY_VALUE_KEY) || StringUtils.isBlank(singleValue(params, USER_PROPERTY_VALUE_KEY)))
        {
            errors.addError(USER_PROPERTY_VALUE_KEY, i18n.getText("automation.action.editProfile.field.empty"));
        }
        return errors;
    }
}
