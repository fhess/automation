package com.atlassian.plugin.automation.jira.util;

import com.atlassian.fugue.Either;
import com.atlassian.jira.bc.issue.comment.CommentService;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Named
public class SecurityLevelContextProvider
{
    public static final String ROLE_PREFIX = "role:";
    public static final String GROUP_PREFIX = "group:";

    private final GroupManager groupManager;
    private final CommentService commentService;
    private final ProjectRoleManager projectRoleManager;
    private final PermissionManager permissionManager;
    private final I18nResolver i18n;

    @Inject
    public SecurityLevelContextProvider(
            @ComponentImport final GroupManager groupManager,
            @ComponentImport final CommentService commentService,
            @ComponentImport final ProjectRoleManager projectRoleManager,
            @ComponentImport final PermissionManager permissionManager,
            @ComponentImport final I18nResolver i18n)
    {
        this.groupManager = groupManager;
        this.commentService = commentService;
        this.projectRoleManager = projectRoleManager;
        this.permissionManager = permissionManager;
        this.i18n = i18n;
    }

    /**
     * Given a user and project this populates the context necessary to render a comment security level dropdown.
     *
     * @param user The user configuring the dropdown
     * @param project A project for which to look up roles. Can be null in which case all projects the user can browse
     * will be used.
     * @param currentSelection can be either role id string or group name
     * @return Context for rendering a soy template.
     */
    public Map<String, Object> getContext(final ApplicationUser user, final Project project, final String currentSelection)
    {
        final Map<String, Object> ret = Maps.newHashMap();
        final Iterable<String> groupLevels = getGroupLevels(user);
        final Iterable<ProjectRole> roleLevels = getRoleLevels(user, project);

        if(commentService.isGroupVisiblityEnabled())
        {
            ret.put("groupLevels", groupLevels);
        }
        if(commentService.isProjectRoleVisiblityEnabled())
        {
            ret.put("roleLevels", roleLevels);
        }

        boolean locked = false;
        String currentText = i18n.getText("security.level.viewable.by.all");
        if (StringUtils.startsWith(currentSelection, GROUP_PREFIX))
        {
            for (String groupLevel : groupLevels)
            {
                if (StringUtils.equals(GROUP_PREFIX + groupLevel, currentSelection))
                {
                    locked = true;
                    currentText = i18n.getText("security.level.restricted.to", groupLevel);
                    ret.put("selectedGroup", groupLevel);
                    break;
                }
            }
        }
        else if (StringUtils.startsWith(currentSelection, ROLE_PREFIX))
        {
            for (ProjectRole roleLevel : roleLevels)
            {
                if (StringUtils.equals(ROLE_PREFIX + Long.toString(roleLevel.getId()), currentSelection))
                {
                    locked = true;
                    currentText = i18n.getText("security.level.restricted.to", roleLevel.getName());
                    ret.put("selectedRoleId", roleLevel.getId());
                    break;
                }
            }
        }
        ret.put("locked", locked);
        ret.put("currentSelection", currentText);

        return ret;
    }

    public ErrorCollection validate(final I18nResolver i18n, final ApplicationUser user, final Project project, final String value)
    {
        final ErrorCollection errors = new ErrorCollection();
        final Either<String, Long> groupOrRole = extractValue(value);
        if (groupOrRole == null)
        {
            errors.addErrorMessage(i18n.getText("automation.jira.commentSecurityLevel.invalid"));
            return errors;
        }

        if (groupOrRole.isLeft())
        {
            final Iterable<String> groups = getGroupLevels(user);
            boolean groupExists = Iterables.contains(groups, groupOrRole.left().get());
            if (!groupExists)
            {
                errors.addErrorMessage(i18n.getText("automation.jira.commentSecurityLevel.invalid"));
            }
        }
        if (groupOrRole.isRight())
        {
            final Iterable<ProjectRole> roleLevels = getRoleLevels(user, project);
            final Long roleId = groupOrRole.right().get();
            boolean roleExists = Iterables.any(roleLevels, new Predicate<ProjectRole>()
            {
                @Override
                public boolean apply(@Nullable final ProjectRole input)
                {
                    return (input != null && input.getId().equals(roleId));
                }
            });
            if (!roleExists)
            {
                errors.addErrorMessage(i18n.getText("automation.jira.commentSecurityLevel.invalid"));
            }
        }

        return errors;
    }

    public Either<String, Long> extractValue(String value)
    {
        if (StringUtils.startsWith(value, GROUP_PREFIX))
        {
            return Either.left(StringUtils.substring(value, GROUP_PREFIX.length()));
        }
        else if (StringUtils.startsWith(value, ROLE_PREFIX))
        {
            final String roleIdString = StringUtils.substring(value, ROLE_PREFIX.length());
            if(!StringUtils.isNumeric(roleIdString))
            {
                return null;
            }
            return Either.right(Long.parseLong(roleIdString));
        }
        else
        {
            return null;
        }
    }

    private Iterable<String> getGroupLevels(final ApplicationUser user)
    {
        final List<String> groupNames = Lists.newArrayList(groupManager.getGroupNamesForUser(user.getName()));
        Collections.sort(groupNames);
        return groupNames;
    }

    private Iterable<ProjectRole> getRoleLevels(final ApplicationUser user, Project project)
    {
        final Set<ProjectRole> ret = Sets.newHashSet();
        if (commentService.isProjectRoleVisiblityEnabled())
        {
            if (project == null)
            {
                final Collection<Project> projectsCanBrowse = permissionManager.getProjects(ProjectPermissions.BROWSE_PROJECTS, user);
                for (Project projectWithBrowse : projectsCanBrowse)
                {
                    ret.addAll(projectRoleManager.getProjectRoles(user, projectWithBrowse));
                }
            }
            else
            {
                ret.addAll(projectRoleManager.getProjectRoles(user, project));
            }
        }
        return ret;
    }
}
