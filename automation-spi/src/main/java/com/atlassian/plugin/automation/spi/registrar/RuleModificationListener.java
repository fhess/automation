package com.atlassian.plugin.automation.spi.registrar;

/**
 * Listener for modifications in the rule configuration which can happen e.g. due to changes on one of the nodes in a cluster
 */
public interface RuleModificationListener
{
    void onRuleModified(int ruleId);
}
