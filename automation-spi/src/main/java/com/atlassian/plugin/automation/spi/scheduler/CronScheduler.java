package com.atlassian.plugin.automation.spi.scheduler;

import com.atlassian.sal.api.scheduling.PluginJob;

import java.util.Map;

/**
 * Extension to SAL's {@link com.atlassian.sal.api.scheduling.PluginScheduler}
 */
public interface CronScheduler
{
    static final String JOB_KEY_RULE_CONTEXT = "KEY_RULE_CONTEXT";
    static final String JOB_KEY_CRON_EXPRESSION = "KEY_CRON_EXPRESSION";

    /**
     * Schedules a job based on a cron expression.  This will eventually be implemented in SAL for all products.
     */
    void scheduleJob(String jobKey, Map<String, Object> jobDataMap, Class<? extends PluginJob> jobClass);

    /**
     * Unschedule the given job. This is mainly here for convenience and will simply call through to SAL's {@link
     * com.atlassian.sal.api.scheduling.PluginScheduler#unscheduleJob(String)}.
     */
    void unscheduleJob(String jobKey);
}
