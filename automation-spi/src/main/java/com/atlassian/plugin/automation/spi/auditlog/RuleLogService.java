package com.atlassian.plugin.automation.spi.auditlog;

import com.atlassian.plugin.automation.core.Rule;

/**
 * Product specific access to the logging the rule
 */
public interface RuleLogService
{
    boolean isAvailable();
    void logRuleExecution(Rule rule, Object associatedItem);
}
