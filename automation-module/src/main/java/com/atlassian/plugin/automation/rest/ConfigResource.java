package com.atlassian.plugin.automation.rest;

import com.atlassian.fugue.Either;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.automation.config.Settings;
import com.atlassian.plugin.automation.core.Action;
import com.atlassian.plugin.automation.core.Rule;
import com.atlassian.plugin.automation.core.Trigger;
import com.atlassian.plugin.automation.core.action.ActionConfiguration;
import com.atlassian.plugin.automation.module.AutomationActionModuleDescriptor;
import com.atlassian.plugin.automation.module.AutomationModuleManager;
import com.atlassian.plugin.automation.module.AutomationTriggerModuleDescriptor;
import com.atlassian.plugin.automation.service.RuleService;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.atlassian.plugin.automation.rest.HandleRuleErrorFunction.NO_CACHE;


/**
 * Provides REST resource for configuration
 */
@Path ("config")
@Produces ({ MediaType.APPLICATION_JSON })
@Consumes ({ MediaType.APPLICATION_JSON })
@WebSudoRequired
public class ConfigResource
{
    private final UserManager userManager;
    private final AutomationModuleManager automationModuleManager;
    private final I18nResolver i18nResolver;
    private final RuleService ruleService;
    private final PluginSettings pluginSettings;

    @Inject
    public ConfigResource(
            @ComponentImport final UserManager userManager,
            @ComponentImport final I18nResolver i18nResolver,
            @ComponentImport final PluginSettingsFactory pluginSettingsFactory,
            final AutomationModuleManager automationModuleManager,
            final RuleService ruleService)
    {
        pluginSettings = pluginSettingsFactory.createGlobalSettings();
        this.userManager = userManager;
        this.automationModuleManager = automationModuleManager;
        this.i18nResolver = i18nResolver;
        this.ruleService = ruleService;
    }

    @GET
    @Path ("/editRuleConfig")
    @Produces ({ MediaType.APPLICATION_JSON })
    public Response getRuleConfig(@Context HttpServletRequest request)
    {
        final String remoteUsername = userManager.getRemoteUsername(request);
        if (!userManager.isSystemAdmin(remoteUsername))
        {
            return Response.status(Response.Status.UNAUTHORIZED).cacheControl(NO_CACHE).build();
        }
        final List<AutomationTriggerModuleDescriptor> triggerDescriptors = Lists.newArrayList(automationModuleManager.getTriggerDescriptors());
        Collections.sort(triggerDescriptors);

        final Map<String, String> triggerMap = Maps.newLinkedHashMap();
        for (AutomationTriggerModuleDescriptor triggerDescriptor : triggerDescriptors)
        {
            triggerMap.put(triggerDescriptor.getCompleteKey(), getModuleName(triggerDescriptor));
        }

        final List<AutomationActionModuleDescriptor> actionDescriptors = Lists.newArrayList(automationModuleManager.getActionDescriptors());
        Collections.sort(actionDescriptors);

        final Map<String, String> actionMap = Maps.newLinkedHashMap();
        for (AutomationActionModuleDescriptor actionModuleDescriptor : actionDescriptors)
        {
            actionMap.put(actionModuleDescriptor.getCompleteKey(), getModuleName(actionModuleDescriptor));
        }
        return Response.ok(new RuleConfig(remoteUsername, triggerMap, actionMap)).cacheControl(NO_CACHE).build();
    }


    @GET
    @Path ("{ruleId}/trigger/{triggerModuleKey}")
    @Produces ({ MediaType.TEXT_HTML })
    public Response getTriggerConfigForm(@Context final HttpServletRequest request, @PathParam ("ruleId") final int ruleId,
            @PathParam ("triggerModuleKey") final String triggerModuleKey, @QueryParam ("actor") final String actor)
    {
        final String remoteUsername = userManager.getRemoteUsername(request);
        if (!userManager.isSystemAdmin(remoteUsername))
        {
            return Response.status(Response.Status.UNAUTHORIZED).cacheControl(NO_CACHE).build();
        }

        final Either<ErrorCollection, Rule> result = ruleService.getRule(remoteUsername, ruleId);
        return result.fold(new HandleErrorFunction(), new Function<Rule, Response>()
        {

            @Override
            public Response apply(@Nullable final Rule input)
            {
                final String currentTriggerModule = input.getTriggerConfiguration().getModuleKey();
                //if we're editing the same trigger render a populate form otherwise render a fresh form
                if (currentTriggerModule.equals(triggerModuleKey))
                {

                    return renderTriggerForm(input, triggerModuleKey, actor);
                }
                else
                {
                    return getNewTriggerConfigForm(request, triggerModuleKey, actor);
                }

            }
        });
    }

    @GET
    @Path ("/trigger/{triggerModuleKey}")
    @Produces ({ MediaType.TEXT_HTML })
    public Response getNewTriggerConfigForm(@Context HttpServletRequest request, @PathParam ("triggerModuleKey") final String triggerModuleKey,
            @QueryParam ("actor") final String actor)
    {
        final String remoteUsername = userManager.getRemoteUsername(request);
        if (!userManager.isSystemAdmin(remoteUsername))
        {
            return Response.status(Response.Status.UNAUTHORIZED).cacheControl(NO_CACHE).build();
        }

        return renderTriggerForm(null, triggerModuleKey, actor);
    }

    @GET
    @Path ("{ruleId}/action/{actionId}/{actionModuleKey}")
    @Produces ({ MediaType.TEXT_HTML })
    public Response getActionConfigForm(@Context final HttpServletRequest request, @PathParam ("ruleId") final int ruleId,
            @PathParam ("actionId") final int actionId, @PathParam ("actionModuleKey") final String actionModuleKey,
            @QueryParam ("actor") final String actor)
    {
        final String remoteUsername = userManager.getRemoteUsername(request);
        if (!userManager.isSystemAdmin(remoteUsername))
        {
            return Response.status(Response.Status.UNAUTHORIZED).cacheControl(NO_CACHE).build();
        }
        final Either<ErrorCollection, Rule> result = ruleService.getRule(remoteUsername, ruleId);
        return result.fold(new HandleErrorFunction(), new Function<Rule, Response>()
        {

            @Override
            public Response apply(@Nullable final Rule input)
            {
                for (ActionConfiguration actionConfiguration : input.getActionsConfiguration())
                {
                    if (actionConfiguration.getId() == actionId)
                    {
                        //if we're editing the same action render a populate form otherwise render a fresh form
                        if (actionConfiguration.getModuleKey().equals(actionModuleKey))
                        {
                            return renderActionForm(actionConfiguration, actionConfiguration.getModuleKey(), actor);
                        }
                        else
                        {
                            return getNewActionConfigForm(request, actionModuleKey, actor);
                        }

                    }
                }
                return Response.status(Response.Status.NOT_FOUND).cacheControl(NO_CACHE).build();
            }
        });
    }

    @GET
    @Path ("/action/{actionModuleKey}")
    @Produces ({ MediaType.TEXT_HTML })
    public Response getNewActionConfigForm(@Context HttpServletRequest request, @PathParam ("actionModuleKey") final String actionModuleKey,
            @QueryParam ("actor") final String actor)
    {
        final String remoteUsername = userManager.getRemoteUsername(request);
        if (!userManager.isSystemAdmin(remoteUsername))
        {
            return Response.status(Response.Status.UNAUTHORIZED).cacheControl(NO_CACHE).build();
        }
        return renderActionForm(null, actionModuleKey, actor);
    }

    @GET
    @Path("/service")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getServiceConfig(@Context HttpServletRequest request)
    {
        final String remoteUsername = userManager.getRemoteUsername(request);
        if (!userManager.isSystemAdmin(remoteUsername))
        {
            return Response.status(Response.Status.UNAUTHORIZED).cacheControl(NO_CACHE).build();
        }
        final boolean isLimiterEnabled = Boolean.valueOf((String) pluginSettings.get(Settings.LIMITER_ENABLED));
        final boolean isProductAuditLogEnabled = Boolean.valueOf((String) pluginSettings.get(Settings.PRODUCT_RULE_LOG_ENABLED));
        final boolean isSyncEventHandlingEnabled = Boolean.valueOf((String) pluginSettings.get(Settings.SYNC_EVENT_HANDLING_ENABLED));
        return Response.ok(new ServiceConfig(isLimiterEnabled, isProductAuditLogEnabled, isSyncEventHandlingEnabled)).build();
    }

    @PUT
    @Path("/service")
    public Response setServiceConfig(@Context HttpServletRequest request, final ServiceConfig serviceConfig)
    {
        final String remoteUsername = userManager.getRemoteUsername(request);
        if (!userManager.isSystemAdmin(remoteUsername))
        {
            return Response.status(Response.Status.UNAUTHORIZED).cacheControl(NO_CACHE).build();
        }
        pluginSettings.put(Settings.LIMITER_ENABLED, String.valueOf(serviceConfig.isRateLimiterEnabled));
        pluginSettings.put(Settings.PRODUCT_RULE_LOG_ENABLED, String.valueOf(serviceConfig.isProductRuleLogEnabled));
        pluginSettings.put(Settings.SYNC_EVENT_HANDLING_ENABLED, String.valueOf(serviceConfig.isSyncEventHandlingEnabled));
        return Response.ok(serviceConfig).build();
    }

    private Response renderActionForm(final ActionConfiguration actionConfiguration, final String moduleKey, final String actor)
    {
        final Action action = automationModuleManager.getAction(moduleKey);
        if (action == null)
        {
            return Response.status(Response.Status.NOT_FOUND).cacheControl(NO_CACHE).build();
        }
        return Response.ok(action.getConfigurationTemplate(actionConfiguration, actor)).cacheControl(NO_CACHE).build();
    }

    private String getModuleName(ModuleDescriptor moduleDescriptor)
    {
        if (StringUtils.isBlank(moduleDescriptor.getI18nNameKey()))
        {
            return moduleDescriptor.getName();
        }
        return i18nResolver.getText(moduleDescriptor.getI18nNameKey());
    }


    private Response renderTriggerForm(final Rule input, final String triggerModuleKey, final String actor)
    {
        final Trigger trigger = automationModuleManager.getTrigger(triggerModuleKey);
        if (trigger == null)
        {
            return Response.status(Response.Status.NOT_FOUND).cacheControl(NO_CACHE).build();
        }
        return Response.ok(trigger.getConfigurationTemplate(input == null ? null : input.getTriggerConfiguration(), actor)).cacheControl(NO_CACHE).build();
    }

    public static class RuleConfig
    {
        @JsonProperty
        private String currentUser;

        @JsonProperty
        private Map<String, String> triggers;

        @JsonProperty
        private Map<String, String> actions;

        public RuleConfig(String currentUser, Map<String, String> triggers, Map<String, String> actions)
        {
            this.currentUser = currentUser;
            this.triggers = triggers;
            this.actions = actions;
        }
    }

    public static class ServiceConfig
    {
        @JsonProperty
        private boolean isRateLimiterEnabled;
        @JsonProperty
        private boolean isProductRuleLogEnabled;
        @JsonProperty
        private boolean isSyncEventHandlingEnabled;

        public ServiceConfig(@JsonProperty("isRateLimiterEnabled") boolean isRateLimiterEnabled,
                             @JsonProperty("isProductRuleLogEnabled") boolean isProductRuleLogEnabled,
                             @JsonProperty("isSyncEventHandlingEnabled") boolean isSyncEventHandlingEnabled)
        {
            this.isRateLimiterEnabled = isRateLimiterEnabled;
            this.isProductRuleLogEnabled = isProductRuleLogEnabled;
            this.isSyncEventHandlingEnabled = isSyncEventHandlingEnabled;
        }
    }
}
