package com.atlassian.plugin.automation.auditlog;

import com.atlassian.fugue.Either;
import com.atlassian.plugin.automation.core.auditlog.AuditMessage;
import com.atlassian.plugin.automation.util.ErrorCollection;

/**
 * Provides access to audit log
 */
public interface AuditLogService
{
    /**
     * @param user authentication user
     * @return count of all audit log messages available.
     */
    int getEntriesCount(String user);

    /**
     * @param user       authentication user
     * @param maxResults the maximum number of entries to return
     * @param startAt    index of the first result (ordered by date desc)
     * @return all entries from the audit log
     */
    Either<ErrorCollection, Iterable<AuditMessage>> getAllEntries(String user, int startAt, int maxResults);

    /**
     * Truncates the log so only new entries are present
     */
    void truncateLog();
}
