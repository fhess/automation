package com.atlassian.plugin.automation.scheduler;

import com.atlassian.plugin.automation.core.Action;
import com.atlassian.plugin.automation.core.Rule;
import com.atlassian.plugin.automation.core.Trigger;
import com.atlassian.plugin.automation.core.TriggerContext;
import com.atlassian.plugin.automation.core.action.ActionConfiguration;
import com.atlassian.plugin.automation.core.auditlog.EventAuditMessageBuilder;
import com.atlassian.plugin.automation.status.RuleStatus;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.google.common.base.Stopwatch;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.concurrent.Callable;

class RuleCallable implements Callable<ErrorCollection>
{
    private static final Logger log = Logger.getLogger(RuleCallable.class);
    private static final int MAX_ERRORS = 3;

    private final RuleCallableContext context;
    private final TriggerContext triggerContext;

    public RuleCallable(final RuleCallableContext context, final TriggerContext triggerContext)
    {
        this.context = context;
        this.triggerContext = triggerContext;
    }

    @Override
    public ErrorCollection call() throws Exception
    {
        final Stopwatch stopwatch = Stopwatch.createStarted();
        final Rule rule = context.getRule();
        log.debug("Executing rule: " + rule.getName());
        final ErrorCollection ruleErrors = new ErrorCollection();

        final Trigger trigger = context.getAutomationModuleManager().getTrigger(rule.getTriggerConfiguration().getModuleKey());
        if (trigger != null)
        {
            try
            {
                trigger.init(rule.getTriggerConfiguration());
                final Iterable<?> items = trigger.getItems(triggerContext, ruleErrors);
                if (!Iterables.isEmpty(items))
                {
                    // We need to check HERE if we can execute the actions - event triggers get triggered on the event class itself,
                    // but that does not necessarily mean they return items too
                    if (!context.getRuleExecutionLimiter().canExecuteActions(rule, trigger.getAuditLog().getString()))
                    {
                        disableRuleAndLog(rule);
                        return ruleErrors;
                    }

                    final List<String> actionLogs = Lists.newArrayList();
                    for (ActionConfiguration actionConfiguration : rule.getActionsConfiguration())
                    {
                        final Action action = context.getAutomationModuleManager().getAction(actionConfiguration.getModuleKey());
                        if (action != null)
                        {
                            action.init(actionConfiguration);
                            action.execute(rule.getActor(), items, ruleErrors);
                            actionLogs.add(action.getAuditLog().getString());
                        }
                        else
                        {

                            log.error("No action with module key '" + actionConfiguration.getModuleKey() +
                                    "' could be loaded for rule '" + rule.getName() + "'. Is the plugin providing this action still installed?");
                            context.getEventAuditLogService().addEntry(new EventAuditMessageBuilder().
                                    setActor(rule.getActor()).
                                    setRuleId(rule.getId()).
                                    setMessage("Invalid configuration").
                                    setErrors("Action module not present: " + actionConfiguration.getModuleKey()).build());
                        }
                    }
                    logStatusAndErrors(rule, ruleErrors, trigger.getAuditLog().getString(), items, actionLogs);
                }
            }
            catch (Exception e)
            {
                log.error("Exception while executing the rule", e);
                context.getEventAuditLogService().addEntry(new EventAuditMessageBuilder().
                        setActor(rule.getActor()).
                        setRuleId(rule.getId()).
                        setMessage("Exception occurred").
                        setErrors(e.getClass().getCanonicalName()).build());
            }
        }
        else
        {
            log.error("No trigger with module key '" + rule.getTriggerConfiguration().getModuleKey() +
                    "' could be loaded for rule '" + rule.getName() + "'. Is the plugin providing this trigger still installed?");
            context.getEventAuditLogService().addEntry(new EventAuditMessageBuilder().
                    setActor(rule.getActor()).
                    setRuleId(rule.getId()).
                    setMessage("Invalid configuration").
                    setErrors("Trigger module not present: " + rule.getTriggerConfiguration().getModuleKey()).build());

        }

        log.debug(String.format("Finished executing rule: %s. Elapsed time: %s", rule.getName(), stopwatch));
        return ruleErrors;
    }

    private void logStatusAndErrors(Rule rule, ErrorCollection ruleErrors, String triggerAuditLog,
                                    Iterable<?> items, List<String> actionLogs)
    {
        // produce some reasonable audit logging
        final String triggerMessage = String.format("%s resulted in %d items.", triggerAuditLog, Iterables.size(items));

        // limit error messages to first MAX_ERRORS instead of getting all of them
        final int lastErrorMessageIdx = Math.min(MAX_ERRORS, ruleErrors.getErrorMessages().size());
        String errorMessages = StringUtils.join(ruleErrors.getErrorMessages().subList(0, lastErrorMessageIdx), ";");

        // we need to get errors as well
        final int lastErrorIdx = Math.min(MAX_ERRORS, ruleErrors.getErrors().size());
        errorMessages += StringUtils.join(Lists.newArrayList(ruleErrors.getErrors().values()).subList(0, lastErrorIdx), ";");

        if (ruleErrors.getErrorMessages().size() > MAX_ERRORS || ruleErrors.getErrors().size() > MAX_ERRORS)
        {
            // we had more errors than stored, so let's just make a note of that
            errorMessages += " There were more errors, logging only first " + (lastErrorMessageIdx + lastErrorIdx);
        }

        final SettingsAwareRuleLogService ruleLogService = context.getSettingsAwareRuleLogService();

        // log in standard audit log if the product one is disabled or if there are errors
        if (!ruleLogService.isEnabled() || StringUtils.isNotBlank(errorMessages))
        {
            context.getEventAuditLogService().addEntry(new EventAuditMessageBuilder().
                    setActor(rule.getActor()).
                    setRuleId(rule.getId()).
                    setMessage("Executed rule").
                    setTriggerMessage(triggerMessage).
                    setActionMessages(actionLogs).
                    setErrors(errorMessages).build());
        }
        if (ruleLogService.isEnabled())
        {
            // log the success state (the error one is logged above with details already)
            if (StringUtils.isBlank(errorMessages))
            {
                context.getEventAuditLogService().addStatusEntry(rule.getId(),RuleStatus.OK);
            }
            // This way we can update all items in 'product-supported' way with short notice on why they were modified
            for (Object item : items)
            {
                ruleLogService.logRuleExecution(rule, item);
            }
        }
    }

    private void disableRuleAndLog(final Rule rule)
    {
        final Rule disabledRule = context.getRuleConfigStore().updateRuleStatus(rule.getId(), false);
        context.getAutomationScheduler().unscheduleRule(rule.getId());
        if (disabledRule.isEnabled())
        {
            context.getEventAuditLogService().addEntry(new EventAuditMessageBuilder().setMessage("Unable to disable rule").setRuleId(rule.getId()).build());
        }
        else
        {
            context.getEventAuditLogService().addEntry(new EventAuditMessageBuilder().setMessage("Disabled rule due to possible loop").setRuleId(rule.getId()).build());
        }
    }
}
