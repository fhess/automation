package com.atlassian.plugin.automation.config;

import com.atlassian.plugin.automation.core.Rule;
import com.atlassian.plugin.automation.core.action.ActionConfiguration;
import com.atlassian.plugin.automation.core.trigger.TriggerConfiguration;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonDeserialize;

import java.util.List;

@JsonIgnoreProperties({"triggerView", "actionViews"})
public class DefaultRule implements Rule
{
    @JsonProperty
    private final String name;

    @JsonProperty
    private Integer id;

    @JsonProperty
    private final String actor;

    @JsonProperty("trigger")
    private final TriggerConfiguration triggerConfiguration;

    @JsonProperty("actions")
    private final Iterable<ActionConfiguration> actionsConfiguration;

    @JsonProperty
    private final boolean enabled;

    @JsonCreator
    public DefaultRule(@JsonProperty("id") final int id,
                       @JsonProperty("name") final String name,
                       @JsonProperty("actor") final String actor,
                       @JsonProperty("trigger")
                       @JsonDeserialize(as = DefaultAutomationConfiguration.class)
                       final TriggerConfiguration trigger,
                       @JsonDeserialize(contentAs = DefaultAutomationConfiguration.class)
                       @JsonProperty("actions") final List<ActionConfiguration> actions,
                       @JsonProperty("enabled") final boolean enabled)
    {
        this.name = name;
        this.id = id;
        this.actor = actor;
        this.triggerConfiguration = trigger;
        this.actionsConfiguration = actions;
        this.enabled = enabled;
    }

    @Override
    public TriggerConfiguration getTriggerConfiguration()
    {
        return triggerConfiguration;
    }

    @Override
    public Iterable<ActionConfiguration> getActionsConfiguration()
    {
        return actionsConfiguration;
    }

    @Override
    public String getName()
    {
        return name;
    }

    @Override
    public String getActor()
    {
        return actor;
    }

    @Override
    public Integer getId()
    {
        return id;
    }

    @Override
    public boolean isEnabled()
    {
        return enabled;
    }

    public DefaultRule unsetAllIds()
    {
        id = null;
        ((DefaultAutomationConfiguration)triggerConfiguration).unsetId();
        for (ActionConfiguration actionConfiguration : actionsConfiguration)
        {
            ((DefaultAutomationConfiguration)actionConfiguration).unsetId();
        }

        return this;
    }
}
