package com.atlassian.plugin.automation.config;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.automation.core.Rule;
import com.atlassian.plugin.automation.core.action.ActionConfiguration;
import com.atlassian.plugin.automation.core.trigger.TriggerConfiguration;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.collect.Maps;
import net.java.ao.DBParam;
import net.java.ao.Query;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.Map;

import static com.atlassian.plugin.automation.config.ao.CurrentSchema.ActionConfigEntity;
import static com.atlassian.plugin.automation.config.ao.CurrentSchema.ActionConfigValueEntity;
import static com.atlassian.plugin.automation.config.ao.CurrentSchema.ActionEntity;
import static com.atlassian.plugin.automation.config.ao.CurrentSchema.RuleEntity;
import static com.atlassian.plugin.automation.config.ao.CurrentSchema.TriggerConfigEntity;
import static com.atlassian.plugin.automation.config.ao.CurrentSchema.TriggerConfigValueEntity;
import static com.atlassian.plugin.automation.config.ao.CurrentSchema.TriggerEntity;
import static com.google.common.collect.Lists.newArrayList;

/**
 * Persists rules in active objects
 */
@Named
public class AORuleConfigStore implements RuleConfigStore
{
    private final ActiveObjects ao;

    @Inject
    public AORuleConfigStore(@ComponentImport final ActiveObjects ao)
    {
        this.ao = ao;
    }

    @Override
    public Iterable<Rule> getRules()
    {
        final List<Rule> rules = newArrayList();
        for (RuleEntity ruleEntity : ao.find(RuleEntity.class, Query.select().order("RULE_NAME ASC")))
        {
            rules.add(transformRule(ruleEntity));
        }
        return rules;
    }

    @Override
    public Rule getRule(final int ruleId)
    {
        final RuleEntity ruleEntity = ao.get(RuleEntity.class, ruleId);
        Rule ret = null;
        if (ruleEntity != null)
        {
            ret = transformRule(ruleEntity);
        }
        return ret;
    }

    @Override
    public Rule getRule(final String name)
    {
        final RuleEntity[] ruleEntity = ao.find(RuleEntity.class, Query.select().where("RULE_NAME = ?", name));
        Rule ret = null;
        if (ruleEntity != null && ruleEntity.length == 1)
        {
            ret = transformRule(ruleEntity[0]);
        }
        return ret;
    }

    @Override
    public Rule addRule(final Rule rule)
    {
        final RuleEntity ruleEntity = ao.create(RuleEntity.class,
                new DBParam("RULE_NAME", rule.getName()),
                new DBParam("ENABLED", rule.isEnabled()),
                new DBParam("ACTOR", rule.getActor()));

        createRuleChildren(rule, ruleEntity);

        return transformRule(ruleEntity);
    }

    @Override
    public Rule delete(final int ruleId)
    {
        final Rule deletedRule = getRule(ruleId);

        final RuleEntity ruleEntity = ao.get(RuleEntity.class, ruleId);
        deleteRuleChildren(ruleEntity);
        ao.delete(ruleEntity);

        return deletedRule;
    }

    @Override
    public Rule updateRuleStatus(int ruleId, boolean status)
    {
        final RuleEntity ruleEntity = ao.get(RuleEntity.class, ruleId);
        ruleEntity.setEnabled(status);
        ruleEntity.save();

        return getRule(ruleId);
    }

    @Override
    public Rule updateRule(Rule rule)
    {
        RuleEntity ruleEntity = ao.get(RuleEntity.class, rule.getId());
        ruleEntity.setEnabled(rule.isEnabled());
        ruleEntity.setRuleName(rule.getName());
        ruleEntity.setActor(rule.getActor());
        ruleEntity.save();

        deleteRuleChildren(ruleEntity);
        createRuleChildren(rule, ruleEntity);

        // refresh the rule entity
        ruleEntity = ao.get(RuleEntity.class, rule.getId());
        return transformRule(ruleEntity);
    }

    private Rule transformRule(RuleEntity ruleEntity)
    {
        List<ActionConfiguration> actions = newArrayList();
        for (ActionEntity actionEntity : ruleEntity.getActions())
        {
            Map<String, List<String>> actionParams = getActionConfigParams(actionEntity.getActionConfiguration());
            actions.add(new DefaultAutomationConfiguration(actionEntity.getID(), actionEntity.getCompleteModuleKey(), actionParams));
        }
        // at some stage we may want to introduce ordering here: https://servicedog.atlassian.net/browse/ATM-69

        TriggerEntity triggerEntity = ruleEntity.getTrigger();

        Map<String, List<String>> triggerParams = getTriggerConfigParams(triggerEntity.getTriggerConfiguration());
        TriggerConfiguration triggerConfiguration;
        triggerConfiguration = new DefaultAutomationConfiguration(triggerEntity.getID(), triggerEntity.getCompleteModuleKey(),
                triggerParams);
        return new DefaultRule(ruleEntity.getID(), ruleEntity.getRuleName(), ruleEntity.getActor(), triggerConfiguration, actions, ruleEntity.getEnabled());
    }

    private Map<String, List<String>> getTriggerConfigParams(final TriggerConfigEntity[] triggerConfigurations)
    {
        final Map<String, List<String>> params = Maps.newHashMap();
        for (TriggerConfigEntity configEntity : triggerConfigurations)
        {
            final List<String> values = newArrayList();
            for (TriggerConfigValueEntity configValueEntity : configEntity.getTriggerConfigValues())
            {
                values.add(configValueEntity.getParamValue());
            }

            params.put(configEntity.getParamKey(), values);
        }
        return params;
    }

    private Map<String, List<String>> getActionConfigParams(final ActionConfigEntity[] actionConfigurations)
    {
        final Map<String, List<String>> params = Maps.newHashMap();
        for (ActionConfigEntity configEntity : actionConfigurations)
        {
            final List<String> values = newArrayList();
            for (ActionConfigValueEntity configValueEntity : configEntity.getActionConfigValues())
            {
                values.add(configValueEntity.getParamValue());
            }

            params.put(configEntity.getParamKey(), values);
        }
        return params;
    }

    private void deleteRuleChildren(RuleEntity ruleEntity)
    {
        final TriggerConfigEntity[] triggerConfigurations = ruleEntity.getTrigger().getTriggerConfiguration();
        for (TriggerConfigEntity triggerConfigEntity : triggerConfigurations)
        {
            ao.delete(triggerConfigEntity.getTriggerConfigValues());
        }
        ao.delete(triggerConfigurations);
        ao.delete(ruleEntity.getTrigger());
        for (ActionEntity actionEntity : ruleEntity.getActions())
        {
            final ActionConfigEntity[] actionConfigurations = actionEntity.getActionConfiguration();
            for (ActionConfigEntity actionConfigEntity : actionConfigurations)
            {
                ao.delete(actionConfigEntity.getActionConfigValues());
            }
            ao.delete(actionConfigurations);
            ao.delete(actionEntity);
        }
    }

    private void createRuleChildren(Rule rule, RuleEntity ruleEntity)
    {
        TriggerConfiguration triggerConfiguration = rule.getTriggerConfiguration();

        TriggerEntity triggerEntity = ao.create(TriggerEntity.class, new DBParam("RULE_ENTITY_ID", ruleEntity.getID()),
                new DBParam("COMPLETE_MODULE_KEY", triggerConfiguration.getModuleKey()));

        storeTriggerConfiguration(triggerConfiguration, new DBParam("TRIGGER_ENTITY_ID", triggerEntity.getID()));

        for (ActionConfiguration action : rule.getActionsConfiguration())
        {
            ActionEntity actionEntity = ao.create(ActionEntity.class, new DBParam("RULE_ENTITY_ID", ruleEntity.getID()),
                    new DBParam("COMPLETE_MODULE_KEY", action.getModuleKey()));
            storeActionConfiguration(action, new DBParam("ACTION_ENTITY_ID", actionEntity.getID()));
        }
    }

    private void storeActionConfiguration(ActionConfiguration configuration, DBParam foreignKey)
    {
        for (Map.Entry<String, List<String>> configEntry : configuration.getParameters().entrySet())
        {
            final ActionConfigEntity configEntity = ao.create(ActionConfigEntity.class, new DBParam("PARAM_KEY", configEntry.getKey()), foreignKey);
            for (String paramValue : configEntry.getValue())
            {
                ao.create(ActionConfigValueEntity.class, new DBParam(ActionConfigEntity.ACTION_CONFIG_ID, configEntity.getID()), new DBParam("PARAM_VALUE", paramValue));
            }
        }
    }

    private void storeTriggerConfiguration(TriggerConfiguration configuration, DBParam foreignKey)
    {
        for (Map.Entry<String, List<String>> configEntry : configuration.getParameters().entrySet())
        {
            final TriggerConfigEntity configEntity = ao.create(TriggerConfigEntity.class, new DBParam("PARAM_KEY", configEntry.getKey()), foreignKey);
            for (String paramValue : configEntry.getValue())
            {
                ao.create(TriggerConfigValueEntity.class, new DBParam(TriggerConfigEntity.TRIGGER_CONFIG_ID, configEntity.getID()), new DBParam("PARAM_VALUE", paramValue));
            }
        }
    }
}
