package com.atlassian.plugin.automation.auditlog;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.fugue.Either;
import com.atlassian.plugin.automation.core.auditlog.AuditMessage;
import com.atlassian.plugin.automation.core.auditlog.DefaultAuditMessage;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserManager;
import com.google.common.collect.Lists;
import net.java.ao.DBParam;
import net.java.ao.EntityStreamCallback;
import net.java.ao.Query;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.List;

import static com.atlassian.plugin.automation.config.ao.CurrentSchema.AdminAuditMessageEntity;

@Named
@ExportAsService
@SuppressWarnings("unused")
public class DefaultAdminAuditLogService implements AdminAuditLogService
{
    private static final Logger log = LoggerFactory.getLogger(DefaultAdminAuditLogService.class);

    private static final int MAX_RESULTS = 1000;

    private final I18nResolver i18n;
    private final ActiveObjects ao;
    private final UserManager userManager;

    @Inject
    public DefaultAdminAuditLogService(
            @ComponentImport final I18nResolver i18n,
            @ComponentImport final ActiveObjects ao,
            @ComponentImport final UserManager userManager)
    {
        this.i18n = i18n;
        this.ao = ao;
        this.userManager = userManager;
    }

    @Override
    public void addAdminEntry(AdminAuditLogType adminAuditLogType, String user, int ruleId)
    {
        final AdminAuditMessageEntity newAuditLog = ao.create(AdminAuditMessageEntity.class, new DBParam("DATE", new Date()),
                new DBParam("ACTOR", user),
                new DBParam("RULE_ID", ruleId),
                new DBParam("TYPE", adminAuditLogType.name()),
                new DBParam("MESSAGE", adminAuditLogType.getDescription() + ": " + ruleId));

        log.info("User {} performed the following Automation admin action for rule id {}: {}", new Object[]{user, ruleId, adminAuditLogType.getDescription()});
    }

    @Override
    public int getEntriesCount(String user)
    {
        if (StringUtils.isBlank(user) || !userManager.isAdmin(user))
        {
            return 0;
        }
        return ao.count(AdminAuditMessageEntity.class);
    }

    @Override
    public Either<ErrorCollection, Iterable<AuditMessage>> getAllEntries(String user, int startAt, int maxResults)
    {
        final ErrorCollection errorCollection = new ErrorCollection();
        if (startAt < 0)
        {
            errorCollection.addErrorMessage(i18n.getText("automation.plugin.audit.log.invalid.start"), ErrorCollection.Reason.VALIDATION_FAILED);
        }

        if (maxResults < 0)
        {
            errorCollection.addErrorMessage(i18n.getText("automation.plugin.audit.log.invalid.max"), ErrorCollection.Reason.VALIDATION_FAILED);
        }

        if (StringUtils.isBlank(user) || !userManager.isAdmin(user))
        {
            errorCollection.addErrorMessage(i18n.getText("perm.denied.desc"), ErrorCollection.Reason.FORBIDDEN);
        }

        if (errorCollection.hasAnyErrors())
        {
            return Either.left(errorCollection);
        }
        int resultsLimit = Math.min(MAX_RESULTS, maxResults);
        final List<AuditMessage> messages = Lists.newArrayList();

        // must list out all select parameters, see https://answers.atlassian.com/questions/44653/error-when-streaming-active-objects-data-with-a-callback for details
        ao.stream(AdminAuditMessageEntity.class, Query.select("ID, DATE, ACTOR, RULE_ID, MESSAGE").offset(startAt).limit(resultsLimit).order("DATE DESC"), new EntityStreamCallback<AdminAuditMessageEntity, Integer>()
        {
            @Override
            public void onRowRead(AdminAuditMessageEntity msgEntity)
            {
                messages.add(new DefaultAuditMessage(msgEntity.getDate(), msgEntity.getActor(), msgEntity.getRuleId(), msgEntity.getMessage()));
            }
        });

        return Either.right((Iterable<AuditMessage>) messages);
    }


    @Override
    public void truncateLog()
    {
        // Get date one year ago
        Date oneYearAgo = DateUtils.addYears(new Date(), -1);
        int deletedEntries = 0;
        log.debug("Deleting admin audit log entries older than {}", oneYearAgo);

        AdminAuditMessageEntity[] auditMessageEntities;
        do
        {
            // deleting log entries in chunks prevents possible OOM exception in case the log entries contains a huge amount of data
            // like in https://bitbucket.org/atlassianlabs/automation/issue/23/oom-due-to-verbose-error-logging-to-the
            auditMessageEntities = ao.find(AdminAuditMessageEntity.class, Query.select().where("DATE < ?", oneYearAgo).limit(MAX_RESULTS));
            for (AdminAuditMessageEntity oldEntity : auditMessageEntities)
            {
                ao.delete(oldEntity);
                deletedEntries++;
            }
            log.debug("Deleted {} audit log entries", auditMessageEntities.length);
        } while (auditMessageEntities.length == MAX_RESULTS);

        log.debug("Deleted audit log entries {}", deletedEntries);
    }

}
