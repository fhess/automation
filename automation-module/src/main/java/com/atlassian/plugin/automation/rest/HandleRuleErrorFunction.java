package com.atlassian.plugin.automation.rest;

import com.atlassian.plugin.automation.service.RuleErrors;
import com.atlassian.plugin.automation.util.ErrorCollection;
import com.google.common.base.Function;

import javax.annotation.Nullable;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Response;
import java.util.Set;

public class HandleRuleErrorFunction implements Function<RuleErrors, Response>
{
    public static final CacheControl NO_CACHE;

    static
    {
        NO_CACHE = new CacheControl();
        NO_CACHE.setNoStore(true);
        NO_CACHE.setNoCache(true);
    }

    @Override
    public Response apply(@Nullable final RuleErrors input)
    {
        if (input == null)
        {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).cacheControl(NO_CACHE).build();
        }
        final Set<ErrorCollection.Reason> reasons = input.getRuleErrors().getReasons();
        if (reasons.contains(ErrorCollection.Reason.NOT_FOUND))
        {
            return Response.status(Response.Status.NOT_FOUND).entity(input).cacheControl(NO_CACHE).build();
        }
        else if (reasons.contains(ErrorCollection.Reason.FORBIDDEN) || reasons.contains(ErrorCollection.Reason.NOT_LOGGED_IN))
        {
            return Response.status(Response.Status.UNAUTHORIZED).entity(input).cacheControl(NO_CACHE).build();
        }
        else
        {
            return Response.status(Response.Status.BAD_REQUEST).entity(input).cacheControl(NO_CACHE).build();
        }
    }
}
