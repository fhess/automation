automation.plugin.title=Automation
automation.plugin.section.title=Automation

automation.plugin.edit.rule=Edit Rule
automation.plugin.add.rule=Add Rule
automation.plugin.import.rule=Import Rule
automation.plugin.add.rule.name=Name of the rule
automation.plugin.add.rule.name.desc=Descriptive name of the rule which will appear for example in the audit log.
automation.plugin.add.rule.actor=Actor of the rule
automation.plugin.add.rule.actor.desc=Actor will be used to execute the rule and will be used when permission/user settings are checked. E.g. when using JQL trigger, his account will be used to determine the timezone for functions such as startOfDay() and permissions to issues, fields and transitions.
automation.plugin.add.rule.enabled=Enable rule once created?
automation.plugin.add.rule.configure.header=Rule Configuration
automation.plugin.add.rule.trigger.header=Select Trigger
automation.plugin.add.rule.confirm.header=Confirm
automation.plugin.add.rule.next=Next

automation.plugin.add.rule.action.header=Add Actions
automation.plugin.add.rule.action.implementation=Choose action

automation.plugin.add.rule.confirm.desc=You are about to add the following automation rule. Please check all the details before hitting save!
automation.plugin.add.rule.confirm.author=This rule will execute as
automation.plugin.add.rule.confirm.triggerSetup=Trigger setup
automation.plugin.add.rule.confirm.actions=Will execute the following actions
automation.plugin.add.rule.confirm.action=Will execute the following action
automation.plugin.add.rule.confirm.enabled=Rule executes once added

automation.plugin.add.rule.action.add=Add action...

automation.plugin.rule.type.cron=CRON schedule
automation.plugin.rule.type.cron.desc=CRON expression (including seconds) like <i>30 * * * * ?</i>. For format description, check <a href="http://quartz-scheduler.org/documentation/quartz-2.2.x/tutorials/crontrigger" target="_blank">this guide</a>. This is scheduled with respect to the current server time: {0}. <b>NOTE:</b> only precisions above 60 seconds are officially be supported.

automation.plugin.rule.type.event=Trigger event
automation.plugin.rule.type.event.desc=Fully qualified event class name (com.atlassian.jira.event.issue.IssueEvent) that will trigger this rule.

automation.plugin.eventlog=Event log
automation.plugin.eventlog.header.errors=Errors
automation.plugin.eventlog.header.triggerMessage=Trigger
automation.plugin.eventlog.header.actionMessages=Actions

automation.plugin.auditlog=Admin log
automation.plugin.auditlog.header.date=Date
automation.plugin.auditlog.header.actor=Actor
automation.plugin.auditlog.header.ruleName=Rule
automation.plugin.auditlog.header.message=Message

automation.plugin.forms.yes=Yes
automation.plugin.forms.no=No
automation.plugin.forms.none=None
automation.plugin.forms.add=Add
automation.plugin.forms.cancel=Cancel
automation.plugin.forms.save=Save

automation.plugin.existing.rules=Existing rules

not.found.desc=Item not found
not.logged.in.desc=User is not logged in
rule.action.missing=Please provide at least one action
rule.trigger.missing=Please provide at valid trigger
rule.name.invalid=Rule name is invalid
rule.actor.invalid=Actor of the rule is invalid
rule.name.exists=Rule with the same name already exists
rule.trigger.invalid=Trigger complete module key is invalid
rule.action.invalid=Action complete module key is invalid
rule.params.invalid=Parameters are invalid
rule.trigger.setup.invalid=Trigger setup is invalid
rule.trigger.cron.invalid=CRON expression is invalid
rule.validation.step.invalid=Validation step is invalid

perm.denied.desc=Permission denied
automation.plugin.error.retrieving.config=Error retrieving the config

automation.plugin.unexpected.errors=Unexpected errors occurred:
automation.plugin.unknown.error=Unknown error
automation.plugin.unknown.error.reload=Unknown error. Please reload and try again.
automation.plugin.error.loading.rule=Error loading rule with id {0}. Please select a different rule to edit.
automation.plugin.error.loading.rules=Error loading automation rules. Please reload the page and try again.
automation.plugin.error.importing.rule=Error occurred while importing rule. Please check server logs. Server response is: '{0}'.

automation.plugin.import.error=There were error(s) while importing rule.
automation.plugin.import.error.header=Following errors occured:

automation.plugin.rule.edit=Edit
automation.plugin.rule.enable=Enable
automation.plugin.rule.disable=Disable
automation.plugin.rule.copy=Copy
automation.plugin.rule.export=Export
automation.plugin.rule.delete=Delete
automation.plugin.delete.confirm=Are you sure you want to delete this item?
automation.plugin.enable.confirm=Are you sure you want to enable this item?
automation.plugin.disable.confirm=Are you sure you want to disable this item?
automation.plugin.action.remove=Remove this action
automation.plugin.action.remove.confirm=Are you sure you want to remove this action?

automation.plugin.rules.header.name=Rule name
automation.plugin.rules.header.actor=Actor
automation.plugin.rules.header.triggerType=Execution
automation.plugin.rules.header.type=Type
automation.plugin.rules.header.trigger=Trigger
automation.plugin.rules.header.actions=Actions
automation.plugin.rules.header.operations=Operations

automation.plugin.rules.cron.detail=Scheduled rules are executed with respect to the server time: {0}

automation.plugin.audit.log.invalid.start = Invalid start parameter specified.
automation.plugin.audit.log.invalid.max = Invalid max results parameter specified.

automation.plugin.no.rules=No rules currently configured.

automation.plugin.config=Configuration
automation.plugin.config.limiter=Rate limiter
automation.plugin.config.limiter.desc=Limit the number of rules executed within short time

automation.plugin.config.rulelog=Rule logging
automation.plugin.config.rulelog.desc=Use product specific rule event logging if possible. This means that only configuration changes and errors will be persisted in the Automation audit logs. Execution of an actual rule will be stored in the corresponding item. In JIRA, this will be added as entity property on the affected issue (see <a href="https://developer.atlassian.com/x/3ICkAQ" target="_blank">this</a>). This makes it searchable using JQL such as "issue.property[automation].last.entry is not EMPTY"

automation.rules.title = Automation Rules
automation.plugin.no.rules.yet=No automation rules have been configured yet.

automation.plugin.status=Status
automation.plugin.status.lozenge.enabled=ENABLED
automation.plugin.status.lozenge.disabled=DISABLED

automation.plugin.status.unknown=Last execution status is UNKNOWN. In clustered environment, the status is not synchronised.
automation.plugin.status.failed=Last execution of the rule resulted in some errors. Check the log for details. In clustered environment, the status is not synchronised.
automation.plugin.status.ok=Last execution of the rule was successful. In clustered environment, the status is not synchronised.

automation.plugin.config.toggle.synchronous=Synchronous processing
automation.plugin.config.toggle.synchronous.desc=Process all event-based rules synchronously. If disabled, event-based rules are processed asynchronously which can improve overall performance. However, if the system is under heavy load, then it is possible for Automation to experience delays in between when events occur and when their triggered rules are executed. Enabling this option will prevent these delays but may decrease overall performance under normal circumstances.