(function () {
    AJS.$(function () {
        AJS.$("#config-form").submit(function (e) {
            e.preventDefault();
            var $this = AJS.$(this);
            $this.find(".aui-icon-success").remove();
            AJS.$(".throbber", $this).addClass('loading');
            var data = {isRateLimiterEnabled: AJS.$('#rate-limiter-enabled', $this).is(':checked'),
                isProductRuleLogEnabled: AJS.$('#rule-log-enabled', $this).is(':checked'),
                isSyncEventHandlingEnabled: AJS.$('#sync-event-handling-enabled', $this).is(':checked')};
            AJS.$.ajax({
                url: $this.attr("action"),
                type: "PUT",
                contentType: "application/json",
                data: JSON.stringify(data),
                success: function() {
                    AJS.$(".throbber", $this).after(Atlassian.Templates.Automation.tickSuccess());
                },
                error: function (resp) {
                    alert(AJS.I18n.getText("automation.plugin.unknown.error"));
                },
                complete: function () {
                    AJS.$(".throbber", $this).removeClass('loading');
                }
            });
        });
    });
})();
