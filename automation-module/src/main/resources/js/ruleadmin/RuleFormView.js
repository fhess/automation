AJS.namespace("Atlassian.Automation.RuleFormView");

Atlassian.Automation.RuleFormView = Brace.View.extend({
    template: Atlassian.Templates.Automation.ruleForm,

    mixins: [Atlassian.Automation.Mixin.ValidationErrors],

    initialize: function () {
        _.bindAll(this, 'render', 'updateModel', 'displayErrors', 'destroy');

        this.model.get("rule").on("change", this.render);
        this.model.on("saveFormState", this.updateModel);
        this.model.on("change:errors", this.displayErrors);
    },

    render: function () {
        this.$el.html(this.template(this.model.get("rule").toJSON()));

        return this;
    },

    updateModel: function () {
        var formParams = this.$("form").serializeObject();
        //checkbox wont be in the formParams so need to explicitly set it if it's not present.
        if (formParams.enabled === undefined) {
            formParams.enabled = false;
        }
        this.model.get("rule").set(formParams);
    },

    displayErrors: function() {
        this.addFormErrors(this.model.get("errors"), "ruleErrors");
    },

    destroy: function () {
        //do proper event cleanup
        this.model.off("saveFormState", this.updateModel);
        this.model.off("change:errors", this.displayErrors);
        this.model.get("rule").off("change", this.render);
        this.undelegateEvents();
    }
});