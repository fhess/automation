package com.atlassian.plugin.automation.page.action;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.plugin.automation.page.ModuleKey;

@ModuleKey ("com.atlassian.plugin.automation.confluence-automation-plugin:create-jira-issue-action")
public class CreateJiraIssueActionForm extends ActionForm
{
    public CreateJiraIssueActionForm(final PageElement container, final String moduleKey)
    {
        super(container, moduleKey);
    }

    public CreateJiraIssueActionForm noUpdatePeriod(final int seconds)
    {
        setActionParam("noUpdatePeriod", String.valueOf(seconds));
        return this;
    }
}
