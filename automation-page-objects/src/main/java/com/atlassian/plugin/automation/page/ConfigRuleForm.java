package com.atlassian.plugin.automation.page;

import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.CheckboxElement;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.plugin.automation.page.util.RuleFormUtils;

import static com.atlassian.jira.pageobjects.form.FormUtils.setElement;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

public class ConfigRuleForm extends AbstractConfigForm
{
    @ElementBy(id = "rule-config-form")
    private PageElement form;

    @ElementBy(id = "rule-name")
    private PageElement ruleName;

    @ElementBy(id = "rule-actor")
    private PageElement ruleActor;

    @ElementBy(id = "rule-enabled")
    private CheckboxElement ruleEnabled;

    @WaitUntil
    public void isAt()
    {
        waitUntilTrue(form.timed().isPresent());
    }

    public ConfigRuleForm ruleName(String name)
    {
        setElement(ruleName, name);
        return this;
    }

    public ConfigRuleForm ruleActor(String actor)
    {
        setElement(ruleActor, actor);
        return this;
    }

    public ConfigRuleForm enabled(boolean isEnabled)
    {
        if (isEnabled)
        {
            ruleEnabled.check();
        }
        else
        {
            ruleEnabled.uncheck();
        }
        return this;
    }

    public TriggerPage next()
    {
        RuleFormUtils.waitForFormRerender();
        nextButton.click();
        return pageBinder.bind(TriggerPage.class);
    }
}
