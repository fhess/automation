package com.atlassian.plugin.automation.page.action;

import com.atlassian.pageobjects.elements.CheckboxElement;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.plugin.automation.page.ModuleKey;
import org.openqa.selenium.By;

@ModuleKey("com.atlassian.plugin.automation.jira-automation-plugin:comment-issue-action")
public class CommentIssueActionForm extends ActionForm
{
    @javax.inject.Inject
    private PageElementFinder elementFinder;

    public CommentIssueActionForm(final PageElement container, final String moduleKey)
    {
        super(container, moduleKey);
    }

    public CommentIssueActionForm comment(final String comment)
    {
        setActionParam("jiraComment", comment);
        return this;
    }

    public CommentIssueActionForm dispatchEvent(final boolean shouldDispatch)
    {

        final CheckboxElement checkbox = container.find(By.name("jiraCommentNotification"), CheckboxElement.class);
        if (shouldDispatch)
        {
            checkbox.check();
        }
        else
        {
            checkbox.uncheck();
        }
        return this;
    }

    public CommentIssueActionForm securityLevel(final String securityLevel)
    {
        final PageElement element = container.find(By.className("drop"));
        element.click();

        final PageElement dropDown = elementFinder.find(By.xpath("//div[@class='ajs-layer select-menu box-shadow active']"));
        final PageElement selectedOption = dropDown.find(By.linkText(securityLevel));
        selectedOption.click();

        return this;
    }
}
