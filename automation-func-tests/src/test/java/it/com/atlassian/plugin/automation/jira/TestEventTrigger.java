package it.com.atlassian.plugin.automation.jira;

import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.client.IssueTypeControl;
import com.atlassian.jira.testkit.client.IssuesControl;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.plugin.automation.page.ActionsForm;
import com.atlassian.plugin.automation.page.AdminPage;
import com.atlassian.plugin.automation.page.CleanupRule;
import com.atlassian.plugin.automation.page.ConfigRuleForm;
import com.atlassian.plugin.automation.page.ConfirmationForm;
import com.atlassian.plugin.automation.page.TriggerPage;
import com.atlassian.plugin.automation.page.action.CommentIssueActionForm;
import com.atlassian.plugin.automation.page.trigger.IssueEventTriggerForm;
import it.com.atlassian.plugin.automation.jira.conditions.IssueCommentedCondition;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestEventTrigger extends AbstractAutomationTest
{
    @Rule
    public CleanupRule cleanup = new CleanupRule(jira());

    @Before
    public void setup()
    {
        backdoor().dataImport().restoreDataFromResource("jira-automation-data.zip");
        backdoor().project().addProject("Another Test", "TESTTWO", "admin");
    }

    @Test
    public void testEventTriggerSyncProcessing()
    {
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

        assertEquals("Some rules are present", 0, adminPage.getRulesCount());
        final ConfigRuleForm configRuleForm = adminPage.addRuleForm();
        final TriggerPage triggerPage = configRuleForm.ruleName("test").
                enabled(true).
                next();
        triggerPage.selectTrigger(IssueEventTriggerForm.class).
                setEvents("Issue Created").
                setEventProcessingSynchronous();

        final ActionsForm actionsForm = triggerPage.next();

        final CommentIssueActionForm actionForm = actionsForm.setInitialAction(CommentIssueActionForm.class);
        actionForm.comment("testsync");
        final ConfirmationForm confirmationForm = actionsForm.next();
        assertTrue("Sync enabled confirmation should be visible", confirmationForm.isIssueEventSyncProcessingEnabled());
        confirmationForm.save();

        final IssueCreateResponse issueInProject = jira().backdoor().issues().createIssue("TEST", "Issue should get a comment");
        String issueKey = issueInProject.key;

        waitUntilTrue(new IssueCommentedCondition(jira().backdoor().issues(), issueKey, 1));

        Issue issue = jira().backdoor().issues().getIssue(issueKey);
        assertEquals("Expected 1 comment", 1, issue.getComments().size());
    }

    @Test
    public void testEventTriggerAsyncProcessing()
    {
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

        assertEquals("Some rules are present", 0, adminPage.getRulesCount());
        final ConfigRuleForm configRuleForm = adminPage.addRuleForm();
        final TriggerPage triggerPage = configRuleForm.ruleName("test").
                enabled(true).
                next();
        triggerPage.selectTrigger(IssueEventTriggerForm.class).
                setEvents("Issue Created").
                setEventProcessingAsynchronous();

        final ActionsForm actionsForm = triggerPage.next();

        final CommentIssueActionForm actionForm = actionsForm.setInitialAction(CommentIssueActionForm.class);
        actionForm.comment("testasync");
        final ConfirmationForm confirmationForm = actionsForm.next();
        assertTrue("Async enabled confirmation should be visible", confirmationForm.isIssueEventAsyncProcessingEnabled());
        confirmationForm.save();

        final IssueCreateResponse issueInProject = jira().backdoor().issues().createIssue("TEST", "Issue should get a comment");
        String issueKey = issueInProject.key;

        waitUntilTrue(new IssueCommentedCondition(jira().backdoor().issues(), issueKey, 1));

        Issue issue = jira().backdoor().issues().getIssue(issueKey);
        assertEquals("Expected 1 comment", 1, issue.getComments().size());
    }

    @Test
    public void testEventTriggerWithJQL()
    {
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

        assertEquals("Some rules are present", 0, adminPage.getRulesCount());
        final ConfigRuleForm configRuleForm = adminPage.addRuleForm();
        final TriggerPage triggerPage = configRuleForm.ruleName("test").
                enabled(true).
                next();
        triggerPage.selectTrigger(IssueEventTriggerForm.class).
                setEvents("Issue Created").
                setJql("project = TEST");
        final ActionsForm actionsForm = triggerPage.next();

        final CommentIssueActionForm actionForm = actionsForm.setInitialAction(CommentIssueActionForm.class);
        actionForm.comment("test");
        actionsForm.next().save();
        
        final IssueCreateResponse issueInProject1 = jira().backdoor().issues().createIssue("TESTTWO", "Issue shouldn't get comments");
        String issue1Key = issueInProject1.key;

        final IssueCreateResponse issueInProject2 = jira().backdoor().issues().createIssue("TEST", "Issue should get comments");
        String issue2Key = issueInProject2.key;

        waitUntilTrue(new IssueCommentedCondition(jira().backdoor().issues(), issue2Key, 1));

        Issue issue1 = jira().backdoor().issues().getIssue(issue1Key);
        assertEquals("Expected 0 comments", 0, issue1.getComments().size());

        Issue issue2 = jira().backdoor().issues().getIssue(issue2Key);
        assertEquals("Expected 1 comments", 1, issue2.getComments().size());
    }

    @Test
    public void testMultipleEventTrigger()
    {
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

        assertEquals("Some rules are present", 0, adminPage.getRulesCount());
        final ConfigRuleForm configRuleForm = adminPage.addRuleForm();
        final TriggerPage triggerPage = configRuleForm.ruleName("test").
                enabled(true).
                next();
        triggerPage.selectTrigger(IssueEventTriggerForm.class).
                setEvents("Issue Created", "Issue Closed");
        ActionsForm actionsForm = triggerPage.next();

        final CommentIssueActionForm actionForm = actionsForm.setInitialAction(CommentIssueActionForm.class);
        actionForm.comment("test");
        actionsForm.next().save();

        final IssueCreateResponse issue = jira().backdoor().issues().createIssue("TEST", "Issue should get comments");
        String issueKey = issue.key;

        waitUntilTrue(new IssueCommentedCondition(jira().backdoor().issues(), issueKey, 1));

        Issue issueObject = jira().backdoor().issues().getIssue(issueKey);
        assertEquals("Expected 1 comments", 1, issueObject.getComments().size());

        jira().goToViewIssue(issueKey).closeIssue();

        waitUntilTrue(new IssueCommentedCondition(jira().backdoor().issues(), issueKey, 2));

        issueObject = jira().backdoor().issues().getIssue(issueKey);
        assertEquals("Expected 2 comments", 2, issueObject.getComments().size());
    }

    @Test
    public void testEventTriggerLimitUser()
    {
        backdoor().usersAndGroups().addUser("fred");
        backdoor().usersAndGroups().addUserToGroup("fred", "jira-users");
        backdoor().usersAndGroups().addUserToGroup("fred", "jira-developers");

        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

        assertEquals("Some rules are present", 0, adminPage.getRulesCount());
        final ConfigRuleForm configRuleForm = adminPage.addRuleForm();
        final TriggerPage triggerPage = configRuleForm.ruleName("test").
                enabled(true).
                next();
        triggerPage.selectTrigger(IssueEventTriggerForm.class).setEvents("Issue Updated").toggleRestrictUsers().toggleAssigneeOnly();
        final ActionsForm actionsForm = triggerPage.next();

        final CommentIssueActionForm actionForm = actionsForm.setInitialAction(CommentIssueActionForm.class);
        actionForm.comment("Issue Updated!");
        actionsForm.next().save();

        final IssueCreateResponse issue = jira().backdoor().issues().createIssue("TEST", "Issue should get comments from fred only!", "fred");
        waitUntilTrue(new IssueCommentedCondition(jira().backdoor().issues(), issue.key, 0));

        //now update the issue again as admin. This should have no effect still 'cause we're logged in as admin!
        backdoor().issues().setDescription(issue.key, "Edited!");
        waitUntilTrue(new IssueCommentedCondition(jira().backdoor().issues(), issue.key, 0));

        //lets do it as fred. This should add a comment!
        IssuesControl fredIssues = new IssuesControl(jira().environmentData(), new IssueTypeControl(jira().environmentData())).loginAs("fred");
        fredIssues.setDescription(issue.key, "Edited by Fred!");
        waitUntilTrue(new IssueCommentedCondition(fredIssues, issue.key, 1));
    }

    @Test
    public void testEventTriggerLimitUserByGroup()
    {
        backdoor().usersAndGroups().addUser("fred");
        backdoor().usersAndGroups().addUserToGroup("fred", "jira-users");
        backdoor().usersAndGroups().addUserToGroup("fred", "jira-developers");

        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

        assertEquals("Some rules are present", 0, adminPage.getRulesCount());
        final ConfigRuleForm configRuleForm = adminPage.addRuleForm();
        final TriggerPage triggerPage = configRuleForm.ruleName("test").
                enabled(true).
                next();
        triggerPage.selectTrigger(IssueEventTriggerForm.class).setEvents("Issue Updated").toggleRestrictUsers().setGroup("jira-administrators");
        final ActionsForm actionsForm = triggerPage.next();

        final CommentIssueActionForm actionForm = actionsForm.setInitialAction(CommentIssueActionForm.class);
        actionForm.comment("Issue Updated!");
        actionsForm.next().save();

        final IssuesControl fredIssues = new IssuesControl(jira().environmentData(), new IssueTypeControl(jira().environmentData())).loginAs("fred");
        final IssueCreateResponse issue = fredIssues.createIssue("TEST", "Original", "fred");
        waitUntilTrue(new IssueCommentedCondition(fredIssues, issue.key, 0));

        //now update the issue as fred. This should have no effect still 'cause fred is not in jira-administrators!
        fredIssues.setDescription(issue.key, "Edited by Fred!");
        waitUntilTrue(new IssueCommentedCondition(fredIssues, issue.key, 0));

        //lets do it as admin. This should add a comment!
        backdoor().issues().setDescription(issue.key, "Edited by Admin!");
        waitUntilTrue(new IssueCommentedCondition(jira().backdoor().issues(), issue.key, 1));
    }
    
}
