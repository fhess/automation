package it.com.atlassian.plugin.automation.jira;

import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.pageobjects.elements.query.AbstractTimedCondition;
import com.atlassian.plugin.automation.page.ActionsForm;
import com.atlassian.plugin.automation.page.AdminPage;
import com.atlassian.plugin.automation.page.CleanupRule;
import com.atlassian.plugin.automation.page.ConfigRuleForm;
import com.atlassian.plugin.automation.page.ConfigurationPage;
import com.atlassian.plugin.automation.page.ConfirmationForm;
import com.atlassian.plugin.automation.page.EventAuditLogPage;
import com.atlassian.plugin.automation.page.ImportRuleFragment;
import com.atlassian.plugin.automation.page.Rule;
import com.atlassian.plugin.automation.page.TriggerPage;
import com.atlassian.plugin.automation.page.action.CommentIssueActionForm;
import com.atlassian.plugin.automation.page.action.TransitionIssueActionForm;
import com.atlassian.plugin.automation.page.trigger.IssueEventTriggerForm;
import com.atlassian.plugin.automation.page.trigger.JqlFilterTriggerForm;
import com.google.common.collect.Iterables;
import it.com.atlassian.plugin.automation.jira.conditions.IssueCommentedCondition;
import it.com.atlassian.plugin.automation.jira.conditions.IssueStateChangedCondition;
import it.com.atlassian.plugin.automation.jira.util.BackdoorCommentUtil;
import it.com.atlassian.plugin.automation.jira.util.IntegrationTestVersionUtil;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.matchers.JUnitMatchers.hasItem;

public class TestAdminServlet extends AbstractAutomationTest
{
    @org.junit.Rule
    public CleanupRule cleanup = new CleanupRule(jira());

    public static final String COMMENT_STRING = "Test me!";

    @Before
    public void setup()
    {
        backdoor().dataImport().restoreDataFromResource("jira-automation-data.zip");
    }

    @Test
    public void testAddRule()
    {
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

        assertEquals("Some rules are present", 0, adminPage.getRulesCount());

        adminPage = addCommentIssueRule(adminPage);
        assertEquals("No rule was added", 1, adminPage.getRulesCount());

        ConfigRuleForm configRuleForm = adminPage.addRuleForm();
        adminPage = configRuleForm.cancel();
        assertEquals("No rule was added", 1, adminPage.getRulesCount());
    }

    @Test
    public void testDeleteRule()
    {
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

        adminPage = addCommentIssueRule(adminPage);

        List<Rule> rules = adminPage.getRules();
        assertEquals("no rules found", 1, rules.size());
        assertEquals(1, rules.get(0).getId());

        rules.get(0).delete();

        int rulesCount = adminPage.getRulesCount();
        assertEquals("some rules found", 0, rulesCount);
    }

    @Test
    public void testUpdateRuleStatus()
    {
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

        adminPage = addCommentIssueRule(adminPage);

        List<Rule> rules = adminPage.getRules();
        assertEquals("no rules found", 1, rules.size());
        assertEquals(1, rules.get(0).getId());
        assertEquals(true, rules.get(0).isEnabled());

        rules.get(0).disable();
        int enabledRulesCount = adminPage.getEnabledRulesCount();
        assertEquals("enabled rules found", 0, enabledRulesCount);
        assertEquals(false, adminPage.getRules().get(0).isEnabled());

        adminPage.getRules().get(0).enable();
        enabledRulesCount = adminPage.getEnabledRulesCount();
        assertEquals("no enabled rules found", 1, enabledRulesCount);
        assertEquals(true, adminPage.getRules().get(0).isEnabled());
    }

    @Test
    public void testEditRule()
    {
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);
        adminPage = addCommentIssueRule(adminPage);

        Rule addedRule = adminPage.getRules().get(0);

        //We don't change anything, only verifying that everything remains the same
        adminPage = addedRule.edit().next().next().next().save();

        final Rule.Trigger trigger = adminPage.getRules().get(0).getTrigger();
        assertEquals("ISSUE COMMENTED", trigger.getParams().get("jiraEventId"));

        final List<Rule.Action> actions = adminPage.getRules().get(0).getActions();
        assertEquals(1, actions.size());
        assertEquals(COMMENT_STRING, actions.get(0).getParams().get("jiraComment"));
        assertFalse(actions.get(0).getParams().containsKey("jiraActionId"));
    }

    @Test
    public void testEditRuleChangeTriggerAndAction()
    {
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

        adminPage = addCommentIssueRule(adminPage);
        assertEquals("Only one rule should be present", 1, adminPage.getRulesCount());

        final IssueCreateResponse createResponse = jira().backdoor().issues().createIssue("TEST", "Test issue");
        final String issueKey = createResponse.key();

        // We need to post comment like this due to bug in backdoor()
        BackdoorCommentUtil.addComment(jira(), issueKey, "Test comment");

        waitUntilTrue(new IssueCommentedCondition(jira().backdoor().issues(), issueKey, 2));

        // Now we need to edit the rule to do something else
        Rule addedRule = adminPage.getRules().get(0);
        final ConfigRuleForm editRuleForm = addedRule.edit();

        final TriggerPage triggerPage = editRuleForm.ruleName("test1").next();
        triggerPage.selectTrigger(JqlFilterTriggerForm.class).
                setCron("30 * * * * ?").
                setJql("project='TEST' AND status='Open'");
        final ActionsForm actionsForm = triggerPage.next();

        final TransitionIssueActionForm transitionActionForm = actionsForm.setInitialAction(TransitionIssueActionForm.class);
        transitionActionForm.selectTransition("2").
                setTransitionFields("resolution=3");

        adminPage = actionsForm.next().save();
        assertEquals("Only one rule should be present", 1, adminPage.getRulesCount());

        // Wait until state change of this issue due to the new trigger
        waitUntilTrue(new IssueStateChangedCondition(jira().backdoor().issues(), issueKey, "Closed"));

        // Only state change should occur, but no other comments should be added
        final int comments = jira().backdoor().issues().getIssue(issueKey).getComments().size();
        assertEquals("Only two comments should be present", 2, comments);
    }

    @Test
    public void testEditRuleAddDeleteAction()
    {
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);
        adminPage = addCommentIssueRule(adminPage);

        final List<Rule.Action> actions = adminPage.getRules().get(0).getActions();
        assertEquals(1, actions.size());
        assertEquals(COMMENT_STRING, actions.get(0).getParams().get("jiraComment"));
        assertFalse(actions.get(0).getParams().containsKey("jiraActionId"));

        // Now we need to edit the rule to do something else
        Rule addedRule = adminPage.getRules().get(0);
        final ActionsForm actionsForm = addedRule.edit().next().next();

        final TransitionIssueActionForm transitionActionForm = actionsForm.addAction(TransitionIssueActionForm.class);
        transitionActionForm.selectTransition("2");

        // Remove action is here because we don't allow to delete the only action in the list, so we first need to add one
        actionsForm.removeAction(0);
        adminPage = actionsForm.next().save();

        final List<Rule.Action> newActions = adminPage.getRules().get(0).getActions();
        assertEquals(1, newActions.size());
        assertFalse(newActions.get(0).getParams().containsKey("jiraComment"));
        assertEquals("classic default workflow: Close Issue (2)", newActions.get(0).getParams().get("jiraActionId"));

        final IssueCreateResponse createResponse = jira().backdoor().issues().createIssue("TEST", "Test issue");
        final String issueKey = createResponse.key();

        BackdoorCommentUtil.addComment(jira(), issueKey, "Test comment");
        waitUntilTrue(new IssueStateChangedCondition(jira().backdoor().issues(), issueKey, "Closed"));

        // Only state change should occur, but no comments should be added
        final int comments = jira().backdoor().issues().getIssue(issueKey).getComments().size();
        assertEquals("Only one comments should be present", 1, comments);
    }

    @Test
    public void testEditRuleAllFields()
    {
        // We'll edit all possible fields
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);
        adminPage = addCommentIssueRule(adminPage);

        // Now we need to edit the rule to do something else
        Rule addedRule = adminPage.getRules().get(0);
        final TriggerPage triggerPage = addedRule.edit().
                ruleName("test1").
                enabled(false).
                next();
        triggerPage.getTrigger(IssueEventTriggerForm.class).setEvents("Issue Closed");
        final ActionsForm actionsForm = triggerPage.next();

        final CommentIssueActionForm actionForm = actionsForm.getInitialAction(CommentIssueActionForm.class);
        actionForm.comment("Test me again!");
        adminPage = actionsForm.next().save();

        Rule editedRule = adminPage.getRules().get(0);
        assertEquals("Invalid rule name", "test1", editedRule.getName());
        assertEquals("Invalid enabled", false, editedRule.isEnabled());
        assertEquals("Invalid trigger", "ISSUE CLOSED", editedRule.getTrigger().getParams().get("jiraEventId"));
        assertEquals("Invalid actions", "Test me again!", editedRule.getActions().get(0).getParams().get("jiraComment"));
    }

    @Test
    public void testCopyRuleAllFields()
    {
        // We'll edit all possible fields of the copied rule
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);
        adminPage = addCommentIssueRule(adminPage);

        // Now we need to copy the rule to do something else
        Rule addedRule = adminPage.getRules().get(0);
        final TriggerPage triggerPage = addedRule.copy().
                ruleName("test1").
                enabled(false).
                next();
        triggerPage.getTrigger(IssueEventTriggerForm.class).setEvents("Issue Closed");
        final ActionsForm actionsForm = triggerPage.next();

        final CommentIssueActionForm actionForm = actionsForm.getInitialAction(CommentIssueActionForm.class);
        actionForm.comment("Test me again!");
        adminPage = actionsForm.next().save();

        assertEquals(adminPage.getRules().size(), 2);

        Rule originalRule = adminPage.getRules().get(0);
        assertEquals("Invalid rule name", "test", originalRule.getName());
        assertEquals("Invalid enabled", true, originalRule.isEnabled());

        Rule copiedRule = adminPage.getRules().get(1);
        assertEquals("Invalid rule name", "test1", copiedRule.getName());
        assertEquals("Invalid enabled", false, copiedRule.isEnabled());
        assertEquals("Invalid trigger", "ISSUE CLOSED", copiedRule.getTrigger().getParams().get("jiraEventId"));
        assertEquals("Invalid actions", "Test me again!", copiedRule.getActions().get(0).getParams().get("jiraComment"));
    }

    @Test
    public void testCopyRuleAndRunIt()
    {
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

        final IssueCreateResponse createResponse = jira().backdoor().issues().createIssue("TEST", "Test issue");
        final String issueKey = createResponse.key();

        TriggerPage triggerPage = adminPage.addRuleForm().
                ruleName("test").
                enabled(false).
                next();

        triggerPage.selectTrigger(IssueEventTriggerForm.class).setEvents("Issue Commented");
        final ActionsForm actionsForm = triggerPage.next();

        final TransitionIssueActionForm transitionActionForm = actionsForm.setInitialAction(TransitionIssueActionForm.class);
        transitionActionForm.selectTransition("2").
                setTransitionFields("resolution=3");

        adminPage = actionsForm.next().save();
        assertEquals("Only one rule should be present", 1, adminPage.getRulesCount());

        //We copy the rule without modifying anything
        Rule addedRule = adminPage.getRules().get(0);
        addedRule.copy().next().next().next().save();

        assertEquals("Two rules should be present", 2, adminPage.getRulesCount());

        adminPage.getRules().get(1).enable();

        BackdoorCommentUtil.addComment(jira(), issueKey, "Test comment");

        // Wait until state change of this issue due to the new trigger
        waitUntilTrue(new IssueStateChangedCondition(jira().backdoor().issues(), issueKey, "Closed"));
    }

    @Test
    public void testRuleValidation()
    {
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);
        ConfigRuleForm configRuleForm = adminPage.addRuleForm();
        final Iterable<String> ruleErrors = configRuleForm.ruleName("").ruleActor("jan").enabled(true).nextExpectError();
        assertThat("invalid rule errors", ruleErrors, hasItem("Rule name is invalid"));
        assertThat("invalid rule errors", ruleErrors, hasItem("Actor of the rule is invalid"));

        final TriggerPage triggerPage = configRuleForm.ruleName("test").ruleActor("admin").next();
        final JqlFilterTriggerForm triggerForm = triggerPage.selectTrigger(JqlFilterTriggerForm.class).
                setCron("").setJql("project='TEST' AND status='Open'");
        Iterable<String> triggerErrors = triggerPage.nextExpectError();
        assertTrue("invalid trigger errors", Iterables.size(triggerErrors) > 0);

        triggerForm.setCron("*/5 * * * * ?").setJql("reporter=now()");
        triggerErrors = triggerPage.nextExpectError();
        assertTrue("invalid trigger errors", Iterables.size(triggerErrors) > 0);

        triggerForm.setJql("created=admin");
        triggerErrors = triggerPage.nextExpectError();
        assertTrue("invalid trigger errors", Iterables.size(triggerErrors) > 0);

        triggerForm.setJql("reporter=N0n3x1st1ngUs3R");
        triggerErrors = triggerPage.nextExpectError();
        assertTrue("invalid trigger errors", Iterables.size(triggerErrors) > 0);

        triggerForm.setJql("project=N0n3x1st1ngPr0j3ct");
        triggerErrors = triggerPage.nextExpectError();
        assertTrue("invalid trigger errors", Iterables.size(triggerErrors) > 0);

        triggerPage.selectTrigger(IssueEventTriggerForm.class).
                setEvents("Issue Commented");
        final ActionsForm actionsForm = triggerPage.next();

        final CommentIssueActionForm commentIssueActionForm = actionsForm.setInitialAction(CommentIssueActionForm.class);
        final TransitionIssueActionForm transitionIssueActionForm = actionsForm.addAction(TransitionIssueActionForm.class);

        final Iterable<String> actionsErrors = actionsForm.nextExpectError();
        assertTrue("invalid actions errors", Iterables.size(actionsErrors) >= 1);

        transitionIssueActionForm.selectTransition("2");
        commentIssueActionForm.comment(COMMENT_STRING);

        adminPage = actionsForm.next().save();

        Rule editedRule = adminPage.getRules().get(0);
        assertEquals("Invalid rule name", "test", editedRule.getName());
        assertEquals("Invalid enabled", true, editedRule.isEnabled());
        assertEquals("Invalid trigger", "ISSUE COMMENTED", editedRule.getTrigger().getParams().get("jiraEventId"));
        assertEquals("Invalid actions", COMMENT_STRING, editedRule.getActions().get(0).getParams().get("jiraComment"));
        assertEquals("Invalid actions", "classic default workflow: Close Issue (2)", editedRule.getActions().get(1).getParams().get("jiraActionId"));
    }

    @Test
    public void testRuleLimiter()
    {
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

        // Create a rule that acts on comment event and comments itself - this should create loop
        final TriggerPage triggerPage = adminPage.addRuleForm().
                ruleName("looping rule").
                enabled(true).
                next();
        triggerPage.
                selectTrigger(IssueEventTriggerForm.class).
                setEvents("Issue Commented");
        final ActionsForm actionsForm = triggerPage.next();

        actionsForm.setInitialAction(CommentIssueActionForm.class).
                comment(COMMENT_STRING).
                dispatchEvent(true);
        adminPage = actionsForm.next().save();

        ConfigurationPage configurationPage = adminPage.configurationPage();
        configurationPage.setRateLimiterEnabled(true).save();

        // Create an issue and comment on it
        final IssueCreateResponse createResponse = jira().backdoor().issues().createIssue("TEST", "Test issue");
        final String issueKey = createResponse.key();

        BackdoorCommentUtil.addComment(jira(), issueKey, "Test comment");

        // Wait until all rules have been disabled
        adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);
        waitUntilTrue(new AllRulesDisabledCondition(adminPage));
    }

    @Test
    public void testProductRuleLog() throws Exception
    {
        final int buildNumber = IntegrationTestVersionUtil.getJiraBuildNumber(jira().environmentData().getBaseUrl().toString());
        final ConfigurationPage configurationPage = jira().gotoLoginPage().loginAsSysAdmin(ConfigurationPage.class);

        if (buildNumber >= IntegrationTestVersionUtil.JIRA_62_BUILD_NUMBER)
        {
            assertTrue("this should be supported", configurationPage.isRuleLogOptionVisible());
            configurationPage.setRuleLog(true).save();

            AdminPage adminPage = jira().goTo(AdminPage.class);
            addCommentIssueRule(adminPage);
            EventAuditLogPage eventAuditLogPage = jira().goTo(EventAuditLogPage.class);
            final int initialAuditLogCount = eventAuditLogPage.getLogCount();

            // Create an issue and comment on it
            final IssueCreateResponse createResponse = jira().backdoor().issues().createIssue("TEST", "Test issue");
            final String issueKey = createResponse.key();

            BackdoorCommentUtil.addComment(jira(), issueKey, "Test comment");
            waitUntilTrue("no extra comment added", new IssueCommentedCondition(jira().backdoor().issues(), issueKey, 2));

            // check that no normal audit log was added
            eventAuditLogPage = jira().goTo(EventAuditLogPage.class);
            final int currentAuditLogCount = eventAuditLogPage.getLogCount();
            assertEquals("no audit log should be added", initialAuditLogCount, currentAuditLogCount);

            //TODO this doesn't really work now :(
            /*final SearchResult searchResult = jira().backdoor().search().getSearch(new SearchRequest().jql("issue.property[automation].last.entry ~ test"));
            assertEquals("should match 1 issue", 1, searchResult.issues.size());
            assertEquals("should match " + issueKey, issueKey, searchResult.issues.get(0).key);*/
        }
        else
        {
            assertFalse("this option shouldn't be supported", configurationPage.isRuleLogOptionVisible());
        }
    }

    @Test
    public void testImportExport()
    {
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

        adminPage = addCommentIssueRule(adminPage);
        final String absFilename = adminPage.getRules().get(0).export();

        ImportRuleFragment importFragment = pageBinder().bind(ImportRuleFragment.class);
        importFragment.importRule(absFilename);

        // expect an error because rule with the same name already exists
        waitUntilTrue("import should fail", importFragment.importFailed());

        adminPage = jira().goTo(AdminPage.class);
        adminPage.deleteAllRules();

        // now the import should succeed, which will result to redirect to rule admin page
        jira().goTo(ConfigurationPage.class);
        importFragment = pageBinder().bind(ImportRuleFragment.class);
        importFragment.importRule(absFilename);

        adminPage = pageBinder().bind(AdminPage.class);
        assertEquals("test", adminPage.getRules().get(0).getName());
        assertEquals("Issue Event Trigger", adminPage.getRules().get(0).getTrigger().getType());
    }

    private AdminPage addCommentIssueRule(AdminPage adminPage)
    {
        TriggerPage triggerPage = adminPage.addRuleForm().
                ruleName("test").
                enabled(true).
                next();

        triggerPage.selectTrigger(IssueEventTriggerForm.class).
                setEvents("Issue Commented");
        final ActionsForm actionsForm = triggerPage.next();

        final CommentIssueActionForm actionForm = actionsForm.setInitialAction(CommentIssueActionForm.class);
        actionForm.comment(COMMENT_STRING);

        ConfirmationForm confirmationForm = actionsForm.next();

        return confirmationForm.save();
    }

    public static class AllRulesDisabledCondition extends AbstractTimedCondition
    {
        private static final int MAX_TIMEOUT = 45000;
        private static final int POLLING_INTERVAL = 500;
        private final AdminPage adminPage;

        public AllRulesDisabledCondition(final AdminPage adminPage)
        {
            super(MAX_TIMEOUT, POLLING_INTERVAL);
            this.adminPage = adminPage;
        }

        @Override
        protected Boolean currentValue()
        {
            return 0 == adminPage.getEnabledRulesCount();
        }
    }
}
