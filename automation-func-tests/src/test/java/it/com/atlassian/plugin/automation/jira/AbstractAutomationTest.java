package it.com.atlassian.plugin.automation.jira;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.testkit.client.Backdoor;
import com.atlassian.jira.testkit.client.rules.WebSudoRule;
import com.atlassian.jira.testkit.client.util.TestKitLocalEnvironmentData;
import com.atlassian.jira.tests.pageobjects.DefaultProductInstance;
import com.atlassian.jira.tests.rules.MaximizeWindow;
import com.atlassian.jira.tests.rules.WebDriverScreenshot;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.webdriver.testing.rule.SessionCleanupRule;
import org.junit.Rule;

import java.net.URI;
import java.util.Properties;

public abstract class AbstractAutomationTest {
    private static JiraTestedProduct jira = TestedProductFactory.create(JiraTestedProduct.class,
            new DefaultProductInstance("http://localhost:2990/jira", "jira", 2990, "/jira"), null);

    private static Backdoor backdoor = getBackdoor(jira());

    @Rule
    public static SessionCleanupRule sessionCleanupRule = new SessionCleanupRule();
    @Rule
    public static MaximizeWindow maximizeWindow = new MaximizeWindow();
    @Rule
    public static WebSudoRule webSudoRule = new WebSudoRule(backdoor());
    @Rule
    public static WebDriverScreenshot webDriverScreenshot = new WebDriverScreenshot();

    private static Backdoor getBackdoor(JiraTestedProduct jira) {
        final TestKitLocalEnvironmentData testKitLocalEnvironmentData = getTestKitLocalEnvironmentData(jira);
        return new Backdoor(testKitLocalEnvironmentData);
    }

    private static TestKitLocalEnvironmentData getTestKitLocalEnvironmentData(JiraTestedProduct jira) {
        Properties props = new Properties();
        props.put("jira.port", Integer.toString(jira.getProductInstance().getHttpPort()));
        props.put("jira.context", jira.getProductInstance().getContextPath());
        props.put("jira.host", URI.create(jira.getProductInstance().getBaseUrl()).getHost());
        props.put("jira.xml.data.location", ".");
        return new TestKitLocalEnvironmentData(props, null);
    }

    public static JiraTestedProduct jira() {
        return jira;
    }

    public static Backdoor backdoor() {
        return backdoor;
    }

    public static PageBinder pageBinder() {
        return jira().getPageBinder();
    }
}