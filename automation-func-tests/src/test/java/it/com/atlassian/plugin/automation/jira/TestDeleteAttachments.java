package it.com.atlassian.plugin.automation.jira;

import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.plugin.automation.page.ActionsForm;
import com.atlassian.plugin.automation.page.AdminPage;
import com.atlassian.plugin.automation.page.CleanupRule;
import com.atlassian.plugin.automation.page.TriggerPage;
import com.atlassian.plugin.automation.page.action.DeleteAttachmentsActionForm;
import com.atlassian.plugin.automation.page.action.EditLabelsActionForm;
import com.atlassian.plugin.automation.page.trigger.JqlFilterTriggerForm;
import com.google.common.collect.ImmutableSet;
import it.com.atlassian.plugin.automation.jira.conditions.IssueLabelsPresentCondition;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestDeleteAttachments extends AbstractAutomationTest {
    @org.junit.Rule
    public CleanupRule cleanup = new CleanupRule(jira());

    @Before
    public void setup() {
        jira().backdoor().dataImport().restoreDataFromResource("attach-instance.zip");
    }

    @Test
    public void testDeleteAttachmentsAction() {
        List<String> issueNames = Arrays.asList("WR-1", "WR-2", "WHO-1", "WHO-2");
        List<String> attachmentNames = Arrays.asList("stacktrace.txt", "hipchatlog.txt", "stacktrace.txt", "hipchatlog.txt");

        // sanity check
        for (String issueName : issueNames) {
            assertFalse(issueName + " should have an attachment!", jira().backdoor().issues().
                    getIssue(issueName).fields.attachment.isEmpty());
        }

        AdminPage automationAdmin = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

        final TriggerPage triggerPage = automationAdmin.addRuleForm()
                .enabled(true)
                .ruleName("test")
                .ruleActor("admin")
                .next();

        final String processedLabel = "processed";
        triggerPage.selectTrigger(JqlFilterTriggerForm.class)
                .setCron("*/5 * * * * ?")
                .setJql("labels not in (\"" + processedLabel + "\") or labels is EMPTY order by issueKey DESC");

        ActionsForm actionsForm = triggerPage.next();
        actionsForm.
                setInitialAction(DeleteAttachmentsActionForm.class).
                ignoredAttachmentsPattern("?ipchat*.txt");
        final ImmutableSet<String> addedLabel = ImmutableSet.of(processedLabel);
        actionsForm.addAction(EditLabelsActionForm.class).labelsToAdd(addedLabel).dispatchEvent(true);
        actionsForm.next().save();

        // All is finished when the issue does have the label
        Poller.waitUntilTrue("Delete attachments action didn't run!", new IssueLabelsPresentCondition(jira().backdoor().issues(), "WR-2", addedLabel));

        for (String issueName : issueNames) {
            String attachmentName = attachmentNames.get(issueNames.indexOf(issueName));
            Issue issue = jira().backdoor().issues().getIssue(issueName);
            assertTrue("'processed_attachments' label was not added to issue: " + issueName, issue.fields.labels.contains(processedLabel));

            if (attachmentName.equals("hipchatlog.txt")) {
                assertEquals("Attachments were not deleted in issue: " + issueName, 1, issue.fields.attachment.size());
                assertEquals("Comment added on issue: " + issueName, 0, issue.getComments().size());
            }
            else {
                assertEquals("Attachments were not deleted in issue: " + issueName, 0, issue.fields.attachment.size());
                assertTrue("Comment not added on issue: " + issueName, issue.getComments().get(0).body.contains(attachmentName));
            }
        }
    }
}
