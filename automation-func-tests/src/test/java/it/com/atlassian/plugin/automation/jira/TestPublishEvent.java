package it.com.atlassian.plugin.automation.jira;

import com.atlassian.plugin.automation.page.ActionsForm;
import com.atlassian.plugin.automation.page.AdminPage;
import com.atlassian.plugin.automation.page.CleanupRule;
import com.atlassian.plugin.automation.page.TriggerPage;
import com.atlassian.plugin.automation.page.action.CommentIssueActionForm;
import com.atlassian.plugin.automation.page.action.PublishEventActionForm;
import com.atlassian.plugin.automation.page.trigger.IssueEventTriggerForm;
import it.com.atlassian.plugin.automation.jira.conditions.IssueCommentedCondition;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.junit.Assert.assertEquals;

public class TestPublishEvent extends AbstractAutomationTest
{
    @org.junit.Rule
    public CleanupRule cleanup = new CleanupRule(jira());

    @Before
    public void setup()
    {
        jira().backdoor().dataImport().restoreDataFromResource("jira-automation-data.zip");
    }

    @Test
    public void testPublishEvent()
    {
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);
        assertEquals("Some rules are present", 0, adminPage.getRulesCount());

        TriggerPage triggerPage = adminPage.addRuleForm().
                ruleName("publish event").
                enabled(true).
                next();

        triggerPage.selectTrigger(IssueEventTriggerForm.class).
                setEvents("Issue Created");
        ActionsForm actionsForm = triggerPage.next();
        final String publishedEventName = "Issue Assigned";
        actionsForm.setInitialAction(PublishEventActionForm.class)
                .setEvent(publishedEventName);
        adminPage = actionsForm.next().save();

        // configure another rule which will comment on in case a specific event is published
        triggerPage = adminPage.addRuleForm().
                ruleName("catch event").
                enabled(true).
                next();

        triggerPage.selectTrigger(IssueEventTriggerForm.class).
                setEvents(publishedEventName);
        actionsForm = triggerPage.next();
        actionsForm.setInitialAction(CommentIssueActionForm.class)
                .comment("Caught it!");
        adminPage = actionsForm.next().save();

        assertEquals("No rule was added", 2, adminPage.getRulesCount());

        final String issueKey = jira().backdoor().issues().createIssue("TEST", "Hello").key();

        // comment should be added due to the publish event action
        waitUntilTrue("comment should be added", new IssueCommentedCondition(jira().backdoor().issues(), issueKey, 1));
    }
}
