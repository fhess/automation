package it.com.atlassian.plugin.automation.jira;

import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.client.restclient.Comment;
import com.atlassian.jira.testkit.client.restclient.CommentClient;
import com.atlassian.plugin.automation.page.ActionsForm;
import com.atlassian.plugin.automation.page.AdminPage;
import com.atlassian.plugin.automation.page.CleanupRule;
import com.atlassian.plugin.automation.page.TriggerPage;
import com.atlassian.plugin.automation.page.action.SetAssigneeToLastCommentedActionForm;
import com.atlassian.plugin.automation.page.trigger.IssueEventTriggerForm;
import it.com.atlassian.plugin.automation.jira.conditions.IssueAssigneeCondition;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static junit.framework.Assert.assertNull;
import static org.junit.Assert.assertEquals;

public class TestSetLastCommented extends AbstractAutomationTest
{
    @org.junit.Rule
    public CleanupRule cleanup = new CleanupRule(jira());
    private String issueKey;

    @Before
    public void setup()
    {
        backdoor().dataImport().restoreDataFromResource("jira-automation-data.zip");

        backdoor().usersAndGroups().addUser("fred");
        backdoor().usersAndGroups().addUserToGroup("fred", "jira-users");
        backdoor().usersAndGroups().addUserToGroup("fred", "jira-developers");

        final IssueCreateResponse issue = jira().backdoor().issues().createIssue("TEST", "Hello World", null);
        issueKey = issue.key;

        //create two comments. one from admin and one from fred.
        final CommentClient commentClient = new CommentClient(jira().environmentData());
        final Comment adminComment = new Comment();
        adminComment.body = "Test comment from Admin";
        commentClient.post(issueKey, adminComment);

        final CommentClient fredCommentClient = new CommentClient(jira().environmentData()).loginAs("fred");
        final Comment fredComment = new Comment();
        fredComment.body = "Test comment from Fred";
        fredCommentClient.post(issueKey, fredComment);
    }

    @Test
    public void testLastCommentedWithGroupRestriction()
    {
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

        TriggerPage triggerPage = adminPage.addRuleForm().
                ruleName("test").
                enabled(true).
                next();

        //listen for issue edited events.
        triggerPage.selectTrigger(IssueEventTriggerForm.class).
                setEvents("Issue Updated");
        final ActionsForm actionsForm = triggerPage.next();

        final SetAssigneeToLastCommentedActionForm actionForm = actionsForm.setInitialAction(SetAssigneeToLastCommentedActionForm.class);
        actionForm.selectGroupRestriction("jira-administrators");

        adminPage = actionsForm.next().save();
        assertEquals("No rule was added", 1, adminPage.getRulesCount());


        //check the issue is unassigned!
        assertNull(backdoor().issues().getIssue(issueKey).fields.assignee);

        //lets trigger the issue edited event.
        backdoor().issues().setDescription(issueKey, "add a description to trigger edited event!");

        //assignee should be admin since fred isn't part of the administrators group.
        waitUntilTrue(new IssueAssigneeCondition(jira().backdoor().issues(), issueKey, "admin"));
    }

    @Test
    public void testLastCommentedWithRestrictedUsers()
    {
        //There are two comments in the issue, one from admin, and another from fred
        //We add fred as an excluded user, then we expect that the issue gets assigned to admin

        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

        TriggerPage triggerPage = adminPage.addRuleForm().
                ruleName("test").
                enabled(true).
                next();

        //listen for issue edited events.
        triggerPage.selectTrigger(IssueEventTriggerForm.class).setEvents("Issue Updated");

        final ActionsForm actionsForm = triggerPage.next();

        final SetAssigneeToLastCommentedActionForm actionForm = actionsForm.setInitialAction(SetAssigneeToLastCommentedActionForm.class);
        actionForm.setRestrictedUsers("fred");

        adminPage = actionsForm.next().save();
        assertEquals("No rule was added", 1, adminPage.getRulesCount());


        //check the issue is unassigned!
        assertNull(backdoor().issues().getIssue(issueKey).fields.assignee);

        //lets trigger the issue edited event.
        backdoor().issues().setDescription(issueKey, "add a description to trigger edited event!");

        //assignee should be admin since fred isn't part of the administrators group.
        waitUntilTrue(new IssueAssigneeCondition(backdoor().issues(), issueKey, "admin"));
    }

    @Test
    public void testLastCommented()
    {
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

        TriggerPage triggerPage = adminPage.addRuleForm().
                ruleName("test").
                enabled(true).
                next();

        //listen for issue edited events.
        triggerPage.selectTrigger(IssueEventTriggerForm.class).
                setEvents("Issue Updated");
        final ActionsForm actionsForm = triggerPage.next();

        actionsForm.setInitialAction(SetAssigneeToLastCommentedActionForm.class);
        adminPage = actionsForm.next().save();
        assertEquals("No rule was added", 1, adminPage.getRulesCount());


        //check the issue is unassigned!
        assertNull(backdoor().issues().getIssue(issueKey).fields.assignee);

        //lets trigger the issue edited event.
        backdoor().issues().setDescription(issueKey, "add a description to trigger edited event!");

        //assignee should be fred now since he's the last person to comment.
        waitUntilTrue(new IssueAssigneeCondition(backdoor().issues(), issueKey, "fred"));
    }
}
