package it.com.atlassian.plugin.automation.jira;

import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.plugin.automation.page.ActionsForm;
import com.atlassian.plugin.automation.page.AdminPage;
import com.atlassian.plugin.automation.page.CleanupRule;
import com.atlassian.plugin.automation.page.TriggerPage;
import com.atlassian.plugin.automation.page.action.EditLabelsActionForm;
import com.atlassian.plugin.automation.page.trigger.IssueEventTriggerForm;
import com.google.common.collect.Sets;
import it.com.atlassian.plugin.automation.jira.conditions.IssueLabelsPresentCondition;
import org.junit.Before;
import org.junit.Test;

import java.util.Set;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestEditLabels extends AbstractAutomationTest
{
    @org.junit.Rule
    public CleanupRule cleanup = new CleanupRule(jira());
    private String issueKey;

    @Before
    public void setup()
    {
        jira().backdoor().dataImport().restoreDataFromResource("jira-automation-data.zip");
        jira().backdoor().project().addProject("Another Test", "TESTTWO", "admin");

        final IssueCreateResponse issueResponse = jira().backdoor().issues().createIssue("TESTTWO", "Hello World");
        issueKey = issueResponse.key;
    }

    @Test
    public void testAddRemoveLabel()
    {
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);
        assertEquals("Some rules are present", 0, adminPage.getRulesCount());

        final TriggerPage triggerPage = adminPage.addRuleForm().
                ruleName("test").
                enabled(true).
                next();

        triggerPage.selectTrigger(IssueEventTriggerForm.class).
                setEvents("Issue Updated");
        final ActionsForm actionsForm = triggerPage.next();

        final Set<String> addedLabels = Sets.newHashSet("label1", "label2");
        final String label3 = "label3";
        final Set<String> removedLabels = Sets.newHashSet(label3);
        actionsForm.setInitialAction(EditLabelsActionForm.class)
                .labelsToAdd(addedLabels)
                .labelsToRemove(removedLabels)
                .dispatchEvent(false);
        adminPage = actionsForm.next().save();
        assertEquals("No rule was added", 1, adminPage.getRulesCount());

        //check the labels are empty
        assertTrue("no labels should be present", jira().backdoor().issues().getIssue(issueKey).fields.labels.isEmpty());

        //lets trigger the issue edited event.
        jira().backdoor().issues().setDescription(issueKey, "add a description to trigger edited event!");

        //labels should be added
        waitUntilTrue("labels should be added", new IssueLabelsPresentCondition(jira().backdoor().issues(), issueKey, addedLabels));
        assertFalse("labels were present", jira().backdoor().issues().getIssue(issueKey).fields.labels.containsAll(removedLabels));

        // now add label so it can be removed
        jira().backdoor().issues().addLabel(issueKey, label3);

        // and trigger Issue Updated event
        jira().backdoor().issues().setDescription(issueKey, "add a new description to trigger edited event!");

        waitUntilFalse("label should be removed now", new IssueLabelsPresentCondition(jira().backdoor().issues(), issueKey, removedLabels));
    }
}
