package it.com.atlassian.plugin.automation.jira;

import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.plugin.automation.page.ActionsForm;
import com.atlassian.plugin.automation.page.AdminPage;
import com.atlassian.plugin.automation.page.CleanupRule;
import com.atlassian.plugin.automation.page.TriggerPage;
import com.atlassian.plugin.automation.page.action.EditIssueActionForm;
import com.atlassian.plugin.automation.page.trigger.IssueEventTriggerForm;
import it.com.atlassian.plugin.automation.jira.conditions.IssueFieldChangedCondition;
import it.com.atlassian.plugin.automation.jira.util.BackdoorCommentUtil;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.junit.Assert.assertEquals;

public class TestEditIssue extends AbstractAutomationTest
{

    @Rule
    public CleanupRule cleanup = new CleanupRule(jira());

    @Before
    public void setup()
    {
        jira().backdoor().dataImport().restoreDataFromResource("jira-automation-data.zip");
    }

    @Test
    public void testSimpleEdit()
    {

        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);
        assertEquals("Some rules are present", 0, adminPage.getRulesCount());

        final TriggerPage triggerPage = adminPage.addRuleForm().
                ruleName("test").
                enabled(true).
                next();

        triggerPage.selectTrigger(IssueEventTriggerForm.class).
                setEvents("Issue Created");

        final ActionsForm actionsForm = triggerPage.next();
        actionsForm.setInitialAction(EditIssueActionForm.class).
                setTransitionFields("summary=My summary: $issue.summary").
                enableVariableExpansion(true);
        adminPage = actionsForm.next().save();
        assertEquals("No rule was added", 1, adminPage.getRulesCount());

        final IssueCreateResponse createResponse = jira().backdoor().issues().createIssue("TEST", "Some issue");
        final String issueKey = createResponse.key();

        waitUntilTrue("Summary should be changed", new IssueFieldChangedCondition(jira().backdoor().issues(), issueKey, "summary", "My summary: Some issue"));

    }

    @Test
    public void testCustomFields()
    {

        final String customFieldName = "url_field";
        final String customFieldId = createCustomField(customFieldName, "com.atlassian.jira.plugin.system.customfieldtypes:url");

        final String textFieldName = "text_field";
        final String textFieldId = createCustomField(textFieldName);

        final String floatFieldName = "number_field";
        final String floatFieldId = createCustomField(floatFieldName, "com.atlassian.jira.plugin.system.customfieldtypes:float");

        final String copyFloatFieldName = "number_field";
        final String copyFloatFieldId = createCustomField(copyFloatFieldName, "com.atlassian.jira.plugin.system.customfieldtypes:float");

        final String datePickerFieldName = "date_picker";
        final String datePickerFieldId = createCustomField(datePickerFieldName, "com.atlassian.jira.plugin.system.customfieldtypes:datepicker");

        final IssueCreateResponse createResponse = jira().backdoor().issues().createIssue("TEST", "Some issue");
        final String issueKey = createResponse.key();

        waitUntilTrue("Summary should be changed", new IssueFieldChangedCondition(jira().backdoor().issues(), issueKey, "summary", "Some issue"));

        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);
        assertEquals("Some rules are present", 0, adminPage.getRulesCount());

        final TriggerPage triggerPage = adminPage.addRuleForm().
                ruleName("test").
                enabled(true).
                next();

        triggerPage.selectTrigger(IssueEventTriggerForm.class).setEvents("Issue Commented");

        final String editFields = String.format(
                "summary=$customfields.get(%s) - $issue.summary\n" +
                "%s=$customfields.get(%s) $customfields.get(%s) $customfields.get(%s)\n" +
                "%s=$customfields.get(%s)\n",
                cleanCustomFieldId(customFieldId),
                textFieldId,
                cleanCustomFieldId(customFieldId),
                cleanCustomFieldId(floatFieldId),
                cleanCustomFieldId(datePickerFieldId),
                copyFloatFieldId,
                cleanCustomFieldId(floatFieldId)
        );

        final ActionsForm actionsForm = triggerPage.next();
        actionsForm.setInitialAction(EditIssueActionForm.class).
                setTransitionFields(editFields).
                enableVariableExpansion(true);
        adminPage = actionsForm.next().save();
        assertEquals("No rule was added", 1, adminPage.getRulesCount());

        IssueFields issueFields = new IssueFields();
        issueFields.summary("New summary");
        issueFields.customField(Long.parseLong(cleanCustomFieldId(customFieldId), 10), "http://www.atlassian.com");
        issueFields.customField(Long.parseLong(cleanCustomFieldId(floatFieldId), 10), 230.19);
        issueFields.customField(Long.parseLong(cleanCustomFieldId(textFieldId), 10), "");
        issueFields.customField(Long.parseLong(cleanCustomFieldId(datePickerFieldId), 10), "2014-07-21 00:00:00.0");

        jira().backdoor().issues().setIssueFields(issueKey, issueFields);

        BackdoorCommentUtil.addComment(jira(),issueKey, "test comment");

        waitUntilTrue("Summary should be changed", new IssueFieldChangedCondition(jira().backdoor().issues(), issueKey, "summary", "http://www.atlassian.com - New summary"));
        waitUntilTrue("Textfield should be changed", new IssueFieldChangedCondition(jira().backdoor().issues(), issueKey, textFieldId, "http://www.atlassian.com 230.19 2014-07-21 00:00:00.0"));
        waitUntilTrue("Copy float field should be changed", new IssueFieldChangedCondition(jira().backdoor().issues(), issueKey, copyFloatFieldId, 230.19));
    }

    protected String cleanCustomFieldId(final String customFieldId)
    {
        return customFieldId.replace("customfield_", "");
    }

    protected String createCustomField(final String customFieldName)
    {
        return createCustomField(customFieldName, "com.atlassian.jira.plugin.system.customfieldtypes:textfield");
    }

    protected String createCustomField(final String customFieldName, final String type)
    {
        String id = jira().backdoor().customFields().createCustomField(customFieldName, "something", type, null);

        backdoor().screens().addFieldToScreen("Default Screen", customFieldName);

        return id;
    }

    @Test
    public void testDisabledVariableExpansion()
    {
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);
        assertEquals("Some rules are present", 0, adminPage.getRulesCount());

        final TriggerPage triggerPage = adminPage.addRuleForm().
                ruleName("test").
                enabled(true).
                next();

        triggerPage.selectTrigger(IssueEventTriggerForm.class).
                setEvents("Issue Created");

        final ActionsForm actionsForm = triggerPage.next();
        actionsForm.setInitialAction(EditIssueActionForm.class).
                setTransitionFields("summary=My summary: $issue.summary").
                enableVariableExpansion(false);

        adminPage = actionsForm.next().save();
        assertEquals("No rule was added", 1, adminPage.getRulesCount());

        final IssueCreateResponse createResponse = jira().backdoor().issues().createIssue("TEST", "Some issue");
        final String issueKey = createResponse.key();

        waitUntilTrue("Summary should be changed", new IssueFieldChangedCondition(jira().backdoor().issues(), issueKey, "summary", "My summary: $issue.summary"));
    }

    @Test
    public void testMultipleEditFields()
    {
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);
        assertEquals("Some rules are present", 0, adminPage.getRulesCount());

        final TriggerPage triggerPage = adminPage.addRuleForm().
                ruleName("test").
                enabled(true).
                next();

        triggerPage.selectTrigger(IssueEventTriggerForm.class).
                setEvents("Issue Created");

        final ActionsForm actionsForm = triggerPage.next();
        actionsForm.setInitialAction(EditIssueActionForm.class).
                setTransitionFields("summary=Summary: $issue.summary\ndescription=Assigned to $issue.assignee.name").
                enableVariableExpansion(true);
        adminPage = actionsForm.next().save();
        assertEquals("No rule was added", 1, adminPage.getRulesCount());

        final IssueCreateResponse createResponse = jira().backdoor().issues().createIssue("TEST", "Some multiple issue", "admin");
        final String issueKey = createResponse.key();

        waitUntilTrue("Summary should be changed", new IssueFieldChangedCondition(jira().backdoor().issues(), issueKey, "summary", "Summary: Some multiple issue"));
        waitUntilTrue("Description should be changed", new IssueFieldChangedCondition(jira().backdoor().issues(), issueKey, "description", "Assigned to admin"));
    }

    @Test
    public void testComplexSyntaxEdit()
    {

        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);
        assertEquals("Some rules are present", 0, adminPage.getRulesCount());

        final TriggerPage triggerPage = adminPage.addRuleForm().
                ruleName("test").
                enabled(true).
                next();

        triggerPage.selectTrigger(IssueEventTriggerForm.class).
                setEvents("Issue Created");

        final ActionsForm actionsForm = triggerPage.next();
        actionsForm.setInitialAction(EditIssueActionForm.class).
                setTransitionFields("summary=For $issue.unknownField $50 $dollar: $issue.summary").
                enableVariableExpansion(true);

        adminPage = actionsForm.next().save();
        assertEquals("No rule was added", 1, adminPage.getRulesCount());

        final IssueCreateResponse createResponse = jira().backdoor().issues().createIssue("TEST", "Some complex issue");
        final String issueKey = createResponse.key();

        waitUntilTrue("Summary should be changed", new IssueFieldChangedCondition(jira().backdoor().issues(), issueKey, "summary", "For $issue.unknownField $50 $dollar: Some complex issue"));

    }

}
