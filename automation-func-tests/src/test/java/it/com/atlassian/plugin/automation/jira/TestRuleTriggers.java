package it.com.atlassian.plugin.automation.jira;

import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.plugin.automation.page.ActionsForm;
import com.atlassian.plugin.automation.page.AdminAuditLogPage;
import com.atlassian.plugin.automation.page.AdminPage;
import com.atlassian.plugin.automation.page.CleanupRule;
import com.atlassian.plugin.automation.page.ConfigRuleForm;
import com.atlassian.plugin.automation.page.EventAuditLogPage;
import com.atlassian.plugin.automation.page.Rule;
import com.atlassian.plugin.automation.page.TriggerPage;
import com.atlassian.plugin.automation.page.action.CommentIssueActionForm;
import com.atlassian.plugin.automation.page.action.EditIssueActionForm;
import com.atlassian.plugin.automation.page.action.TransitionIssueActionForm;
import com.atlassian.plugin.automation.page.trigger.IssueEventTriggerForm;
import com.atlassian.plugin.automation.page.trigger.JqlFilterTriggerForm;
import it.com.atlassian.plugin.automation.jira.conditions.IssueCommentedCondition;
import it.com.atlassian.plugin.automation.jira.conditions.IssueStateChangedCondition;
import it.com.atlassian.plugin.automation.jira.util.BackdoorCommentUtil;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.junit.Assert.assertEquals;

public class TestRuleTriggers extends AbstractAutomationTest
{
    public static final String TEST_COMMENT = "Test comment";
    @org.junit.Rule
    public CleanupRule cleanup = new CleanupRule(jira());

    public static final String ACTION_COMMENT_TEXT = "Test me!";
    private static final String CLOSE_ISSUE_2 = "2";

    @Before
    public void setup()
    {
        backdoor().dataImport().restoreDataFromResource("jira-automation-data.zip");
    }

    @Test
    public void testEventTrigger()
    {
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

        AdminAuditLogPage adminLogPage = adminPage.adminAuditLogPage();
        int adminLogCount = adminLogPage.getLogCount();

        assertEquals("Some rules are present", 0, adminPage.getRulesCount());
        final ConfigRuleForm configRuleForm = adminPage.addRuleForm();
        final TriggerPage triggerPage = configRuleForm.ruleName("test").
                enabled(true).
                next();
        triggerPage.selectTrigger(IssueEventTriggerForm.class).
                setEvents("Issue Commented");
        final ActionsForm actionsForm = triggerPage.next();

        final CommentIssueActionForm actionForm = actionsForm.setInitialAction(CommentIssueActionForm.class);
        actionForm.comment(ACTION_COMMENT_TEXT);
        adminPage = actionsForm.next().save();

        assertEquals(Rule.Status.OK, adminPage.getRules().get(0).getStatus());

        adminLogPage = jira().goTo(AdminAuditLogPage.class);
        assertEquals("Expected an admin log", adminLogCount + 1, adminLogPage.getLogCount());

        EventAuditLogPage eventAuditLogPage = adminPage.eventAuditLogPage();
        int logCount = eventAuditLogPage.getLogCount();
        eventTriggerComment();

        eventAuditLogPage = jira().goTo(EventAuditLogPage.class);
        assertEquals("Expected an event log", logCount + 1, eventAuditLogPage.getLogCount());

        adminPage = jira().goTo(AdminPage.class);
        assertEquals(Rule.Status.OK, adminPage.getRules().get(0).getStatus());
    }

    @Test
    public void testCronTrigger()
    {
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

        AdminAuditLogPage adminLogPage = adminPage.adminAuditLogPage();
        int adminLogCount = adminLogPage.getLogCount();

        assertEquals("Some rules are present", 0, adminPage.getRulesCount());
        final ConfigRuleForm configRuleForm = adminPage.addRuleForm();
        final TriggerPage triggerPage = configRuleForm.ruleName("test").
                enabled(true).
                next();
        triggerPage.selectTrigger(JqlFilterTriggerForm.class).
                setCron("*/5 * * * * ?").
                setJql("project='TEST' AND status='Open'");
        final ActionsForm actionsForm = triggerPage.next();
        final TransitionIssueActionForm actionForm = actionsForm.setInitialAction(TransitionIssueActionForm.class);

        actionForm.selectTransition(CLOSE_ISSUE_2);
        adminPage = actionsForm.next().save();

        adminLogPage = jira().goTo(AdminAuditLogPage.class);
        assertEquals("Expected an admin log", adminLogCount + 1, adminLogPage.getLogCount());

        EventAuditLogPage eventAuditLogPage = adminPage.eventAuditLogPage();
        int eventLogCount = eventAuditLogPage.getLogCount();

        final IssueCreateResponse createResponse = jira().backdoor().issues().createIssue("TEST", "Test issue");
        final String issueKey = createResponse.key();

        waitUntilTrue(new IssueStateChangedCondition(jira().backdoor().issues(), issueKey, "Closed"));

        final Issue.Fields fields = jira().backdoor().issues().getIssue(issueKey).fields;
        assertEquals("Status is invalid", "Closed", fields.status.name());
        assertEquals("Resolution is invalid", null, fields.resolution);

        eventAuditLogPage = jira().goTo(EventAuditLogPage.class);
        assertEquals("Expected an event log", eventLogCount + 1, eventAuditLogPage.getLogCount());
    }

    @Test
    public void testEventTriggerDisabled()
    {
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

        assertEquals("Some rules are present", 0, adminPage.getRulesCount());
        final ConfigRuleForm addRuleForm = adminPage.addRuleForm();
        final TriggerPage triggerPage = addRuleForm.ruleName("test").enabled(true).next();
        triggerPage.selectTrigger(IssueEventTriggerForm.class).
                setEvents("Issue Commented");
        final ActionsForm actionsForm = triggerPage.next();
        final CommentIssueActionForm actionForm = actionsForm.setInitialAction(CommentIssueActionForm.class);
        actionForm.comment(ACTION_COMMENT_TEXT);
        adminPage = actionsForm.next().save();

        Rule rule = adminPage.getRules().get(0);
        rule.disable();
        EventAuditLogPage eventAuditLogPage = adminPage.eventAuditLogPage();
        int logCount = eventAuditLogPage.getLogCount();
        eventTriggerCommentDisabled();

        eventAuditLogPage = jira().goTo(EventAuditLogPage.class);
        assertEquals("Not expecting logs", logCount, eventAuditLogPage.getLogCount());
    }

    @Test
    public void testEventTriggerDeleted()
    {
        AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

        assertEquals("Some rules are present", 0, adminPage.getRulesCount());
        final ConfigRuleForm addRuleForm = adminPage.addRuleForm();
        final TriggerPage triggerPage = addRuleForm.ruleName("test").
                enabled(true).next();
        triggerPage.selectTrigger(IssueEventTriggerForm.class).
                setEvents("Issue Commented");
        final ActionsForm actionsForm = triggerPage.next();

        final CommentIssueActionForm actionForm = actionsForm.setInitialAction(CommentIssueActionForm.class);
        actionForm.comment(ACTION_COMMENT_TEXT);
        adminPage = actionsForm.next().save();

        Rule rule = adminPage.getRules().get(0);
        rule.delete();
        EventAuditLogPage eventAuditLogPage = adminPage.eventAuditLogPage();
        int logCount = eventAuditLogPage.getLogCount();
        eventTriggerCommentDisabled();

        eventAuditLogPage = jira().goTo(EventAuditLogPage.class);
        assertEquals("Not expecting logs", logCount, eventAuditLogPage.getLogCount());
    }

    @Test
    public void testMultipleActions()
    {
        final AdminPage adminPage = jira().gotoLoginPage().loginAsSysAdmin(AdminPage.class);

        assertEquals("Some rules are present", 0, adminPage.getRulesCount());
        final ConfigRuleForm configRuleForm = adminPage.addRuleForm();
        final TriggerPage triggerPage = configRuleForm.ruleName("test").
                enabled(true).next();
        triggerPage.selectTrigger(JqlFilterTriggerForm.class).
                setCron("*/5 * * * * ?").
                setJql("project='TEST' AND status='Open'");
        final ActionsForm actionsForm = triggerPage.next();

        final CommentIssueActionForm commentActionForm = actionsForm.setInitialAction(CommentIssueActionForm.class);
        commentActionForm.comment("Hello $reporter.displayName in $issue.key and $project.name");

        final EditIssueActionForm editIssueActionForm = actionsForm.addAction(EditIssueActionForm.class);
        String fields = "description=Charlie the unicorn is watching you\n" +
                "summary=Don't look at him!";
        editIssueActionForm.setTransitionFields(fields);

        final TransitionIssueActionForm transitionActionForm = actionsForm.addAction(TransitionIssueActionForm.class);
        transitionActionForm.selectTransition(CLOSE_ISSUE_2).
                setTransitionFields("resolution=3");

        actionsForm.next().save();

        EventAuditLogPage eventAuditLogPage = adminPage.eventAuditLogPage();
        int logCount = eventAuditLogPage.getLogCount();

        final IssueCreateResponse createResponse = jira().backdoor().issues().createIssue("TEST", "Test issue");
        final String issueKey = createResponse.key();

        // Expecting 1 comment by 1 actions
        waitUntilTrue(new IssueStateChangedCondition(jira().backdoor().issues(), issueKey, "Closed"));

        final Issue issue = jira().backdoor().issues().getIssue(issueKey);
        assertEquals("Expected 1 comment", 1, issue.getComments().size());
        assertEquals("1st comment different", String.format("Hello %s in %s and %s", "admin", issueKey, "test"), issue.getComments().get(0).body);

        assertEquals("Description is invalid", "Charlie the unicorn is watching you", issue.fields.description);
        assertEquals("Description is invalid", "Don't look at him!", issue.fields.summary);

        assertEquals("Status is invalid", "Closed", issue.fields.status.name());
        assertEquals("Resolution is invalid", "Duplicate", issue.fields.resolution.name);

        eventAuditLogPage = jira().goTo(EventAuditLogPage.class);
        assertEquals("Expected a log", logCount + 1, eventAuditLogPage.getLogCount());
    }

    private void eventTriggerComment()
    {
        final IssueCreateResponse createResponse = jira().backdoor().issues().createIssue("TEST", "Test issue");
        final String issueKey = createResponse.key();
        BackdoorCommentUtil.addComment(jira(), issueKey, TEST_COMMENT);

        waitUntilTrue(new IssueCommentedCondition(jira().backdoor().issues(), issueKey, 2));

        Issue issue = jira().backdoor().issues().getIssue(issueKey);
        assertEquals("Expected 2 comments", 2, issue.getComments().size());

        assertEquals("1st comment different", TEST_COMMENT, issue.getComments().get(0).body);
        assertEquals("2nd comment different", ACTION_COMMENT_TEXT, issue.getComments().get(1).body);

        assertEquals("Last transition invalid", "Open", issue.fields.status.name());
    }

    private void eventTriggerCommentDisabled()
    {
        final IssueCreateResponse createResponse = jira().backdoor().issues().createIssue("TEST", "Test issue");
        final String issueKey = createResponse.key();
        BackdoorCommentUtil.addComment(jira(), issueKey, TEST_COMMENT);

        Issue issue = jira().backdoor().issues().getIssue(issueKey);
        assertEquals("Expected 1 comment", 1, issue.getComments().size());

        assertEquals("1st comment different", TEST_COMMENT, issue.getComments().get(0).body);
        assertEquals("Last transition invalid", "Open", issue.fields.status.name());
    }
}
