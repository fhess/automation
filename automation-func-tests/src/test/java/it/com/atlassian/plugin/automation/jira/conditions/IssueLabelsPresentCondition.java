package it.com.atlassian.plugin.automation.jira.conditions;

import com.atlassian.jira.testkit.client.IssuesControl;
import com.atlassian.pageobjects.elements.query.AbstractTimedCondition;

import java.util.Set;

public class IssueLabelsPresentCondition extends AbstractTimedCondition
{
    private static final int MAX_TIMEOUT = 75000;
    private static final int POLLING_INTERVAL = 500;
    private final IssuesControl issuesControl;
    private final String issueKey;
    private final Set<String> labelsPresent;

    public IssueLabelsPresentCondition(IssuesControl issuesControl, String issueKey, Set<String> labelsPresent)
    {
        super(MAX_TIMEOUT, POLLING_INTERVAL);
        this.issuesControl = issuesControl;
        this.issueKey = issueKey;
        this.labelsPresent = labelsPresent;
    }

    @Override
    protected Boolean currentValue()
    {
        return issuesControl.getIssue(issueKey).fields.labels.containsAll(labelsPresent);
    }
}
