package it.com.atlassian.plugin.automation.jira.conditions;

import com.atlassian.jira.testkit.client.IssuesControl;
import com.atlassian.pageobjects.elements.query.AbstractTimedCondition;

public class IssueCommentedCondition extends AbstractTimedCondition
{
    private static final int MAX_TIMEOUT = 15000;
    private static final int POLLING_INTERVAL = 500;
    private final IssuesControl issuesControl;
    private final String issueKey;
    private final int expectedComments;

    public IssueCommentedCondition(final IssuesControl issuesControl, String issueKey, int expectedComments)
    {
        super(MAX_TIMEOUT, POLLING_INTERVAL);
        this.issuesControl = issuesControl;
        this.issueKey = issueKey;
        this.expectedComments = expectedComments;
    }

    @Override
    protected Boolean currentValue()
    {
        final int comments = issuesControl.getIssue(issueKey).getComments().size();
        return expectedComments == comments;
    }
}
