package com.atlassian.plugin.automation.service;

import com.atlassian.fugue.Either;
import com.atlassian.plugin.automation.core.Rule;
import com.atlassian.plugin.automation.util.ErrorCollection;

/**
 * Provides CRUD operations for automation rules.
 */
public interface RuleService
{
    /**
     * @param loggedInUser the user performing the operation
     * @return either an error collection if there's permission errors or an iterable of currently configured rules.
     */
    Either<ErrorCollection, Iterable<Rule>> getRules(String loggedInUser);

    /**
     * @param loggedInUser the user performing the operation
     * @param ruleId the id of the rule to retrieve
     * @return either an error collection if there's permission errors or the rule with the id provided
     */
    Either<ErrorCollection, Rule> getRule(String loggedInUser, int ruleId);

    /**
     * Validates creating a new rule.  This will ensure the loggedInUser has the correct permissions and will validate
     * the provided trigger parameters and action parameters for each action.
     *
     * @param loggedInUser the user performing the operation
     * @return either an error collection containing any errors or the Rule to be created
     */
    Either<RuleErrors, RuleValidationResult> validateAddRule(String loggedInUser, Rule rule);

    /**
     * Given a Rule, this adds the new rule to storage.
     *
     * @return the newly created Rule
     */
    Rule addRule(RuleValidationResult ruleValidationResult);

    /**
     * @param loggedInUser the user performing the operation
     * @param ruleId the id of the rule to delete
     * @return an error collection if this vails validation
     */
    Either<ErrorCollection, DeleteRuleValidationResult> validateDeleteRule(String loggedInUser, int ruleId);

    /**
     * @param loggedInUser the user performing the operation
     * @param ruleId the id of the rule to enable/disable
     * @return an error collection if this fails validation
     */
    Either<ErrorCollection, UpdateRuleStatusValidationResult> validateUpdateRuleStatus(String loggedInUser, int ruleId);

    /**
     * @param deleteRuleValidationResult
     */
    void deleteRule(DeleteRuleValidationResult deleteRuleValidationResult);

    /**
     * @param updateStatusValidationResult
     */
    Rule updateRuleStatus(UpdateRuleStatusValidationResult updateStatusValidationResult, boolean status);

    /**
     * Validates updates rule
     *
     * @param remoteUsername the user performing the operation
     * @param rule The rule to update. Same validation as add Rule
     */
    Either<RuleErrors, RuleValidationResult> validateUpdateRule(String remoteUsername, Rule rule);


    /**
     * Given a Rule, this updates existing rule to storage.
     *
     * @return the updated Rule
     */
    Rule updateRule(RuleValidationResult ruleValidationResult);

    /**
     * Given a rule this method can be used to perform partial validation. In other words this method will *not* verify
     * that a trigger has been defined and that at least one action has been defined.
     *
     *
     * @param remoteUsername the user performing the action
     * @param rule partial rule to validate
     * @param step
     * @return An error collection
     */
    RuleErrors validatePartialRule(String remoteUsername, Rule rule, int step);

    static class RuleValidationResult
    {
        private final Rule rule;

        public RuleValidationResult(final Rule rule)
        {
            this.rule = rule;
        }

        public Rule getRule()
        {
            return rule;
        }
    }

    static class DeleteRuleValidationResult
    {
        private final int ruleId;

        public DeleteRuleValidationResult(int ruleId)
        {
            this.ruleId = ruleId;
        }

        public int getRuleId()
        {
            return ruleId;
        }
    }

    static class UpdateRuleStatusValidationResult
    {
        private final int ruleId;

        public UpdateRuleStatusValidationResult(int ruleId)
        {
            this.ruleId = ruleId;
        }

        public int getRuleId()
        {
            return ruleId;
        }
    }
}
