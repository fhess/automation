package com.atlassian.plugin.automation.util;

import com.atlassian.plugin.automation.core.AutomationConfiguration;

import java.util.List;
import java.util.Map;

/**
 * Utility to deal with multivalue params.
 */
public class ParameterUtil
{
    public static final String RULE_EVENT_SYNC_ENABLED = "ruleEventSyncEnabled";
    
    public static void transformParams(final Map<String, Object> context, final AutomationConfiguration automationConfiguration)
    {
        if (automationConfiguration != null)
        {
            for (Map.Entry<String, List<String>> param : automationConfiguration.getParameters().entrySet())
            {
                final List<String> value = param.getValue();
                if (value.size() > 1)
                {
                    context.put(param.getKey(), value);
                }
                else if (value.size() == 1)
                {
                    context.put(param.getKey(), value.get(0));
                }
            }
            context.put("id", automationConfiguration.getId());
        }
    }

    public static String singleValue(final AutomationConfiguration config, final String key)
    {
        if (config != null)
        {
            return singleValue(config.getParameters(), key);
        }
        return null;
    }

    public static String singleValue(final Map<String, List<String>> params, final String key)
    {
        if (params != null)
        {
            final List<String> values = params.get(key);
            if (values != null)
            {
                // trick to deal with collections which can contain 'null' values (Iterables.first() didn't deal with it properly)
                for (final String value : values)
                {
                    return value;
                }
            }
        }
        return null;
    }

}
